package com.learning.common.utils.alibaba;

import com.aliyun.oss.ClientException;
import com.aliyun.oss.OSSClient;
import com.aliyun.oss.OSSException;
import com.aliyun.oss.model.PutObjectResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * Aliyun OSS 发起请求工具类
 * </p>
 *
 * @author Kevin
 * @since 2018-3-12
 */
public class AliOSSUtils {

    private final static Logger logger = LoggerFactory.getLogger(AliOSSUtils.class);

    /** oss 区域 域名 */
    private static String endpoint = "http://oss-cn-hangzhou.aliyuncs.com";

    /** oss Access Key ID */
    private static String accessKeyId = "LTAIb7f2Mch7hSpx";

    /** oss Access Key Secret */
    private static String accessKeySecret = "5nQp7XcqBHU7xYqsBpLTqYQigLdvJ4";

    /** oss bucket name */
    private static String bucketName = "base-net";

    /** oss object key images */
    private static String imagesKey="images/";

    /** oss object key video */
    private static String videoKey="video/";

    /** 文件访问oss 对应的域名 bucket-host*/
    private static String FILE_HOST = "https://base-net.oss-cn-hangzhou.aliyuncs.com";

    /**
     * <p>
     *     描述：web端读取oss上传 信息
     *     备注：调用此方法前一定要验证session or token有效性, 验证其用户身份信息
     * </p>
     * @return
     */
    public static Map<String, String> getOssCheckSign() {
        Map<String, String> resultMap = new HashMap<>(6);
        resultMap.put("endpoint", endpoint);
        resultMap.put("accessKeyId", accessKeyId);
        resultMap.put("accessKeySecret", accessKeySecret);
        resultMap.put("bucketName", bucketName);
        resultMap.put("imagesKey", imagesKey);
        resultMap.put("videoKey", videoKey);
        resultMap.put("file_host", FILE_HOST);
        return resultMap;
    }

    /**
     * <p>
     *     描述：文件上传
     * </p>
     * @param file 文件
     * @param type
     *            1. 图片, 2 音频
     * @return
     */
    public static String upload(int type, File file) {
        OSSClient ossClient = new OSSClient(endpoint, accessKeyId, accessKeySecret);
        if (file == null) {
            return null;
        }
        try {
            // 判断Bucket是否存在。
            if (ossClient.doesBucketExist(bucketName)) {
                logger.info("您已经创建Bucket：" + bucketName + "。");
            } else {
                logger.info("您的Bucket不存在，创建Bucket：" + bucketName + "。");
                // 创建Bucket
                ossClient.createBucket(bucketName);
            }
            // 文件原名称
            String fileName = file.getName();// 文件原名称
            // 自定义名称 获取当前时间 重命名 以防上传相同文件 名称冲突
            String trueFileName = String.valueOf(System.currentTimeMillis()) + fileName;

            PutObjectResult result;
            if (type == 1) {
                result = ossClient.putObject(bucketName, imagesKey + trueFileName, file);
            } else {
                result = ossClient.putObject(bucketName, videoKey + trueFileName, file);
            }
            //返回访问url
            if (result != null) {
                if (type == 1) {
                    return FILE_HOST + imagesKey + trueFileName;
                }
                return FILE_HOST + videoKey + trueFileName;
            }
        } catch (OSSException oe) {
            oe.printStackTrace();
        } catch (ClientException ce) {
            ce.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            ossClient.shutdown();
        }
        return "FINISH";
    }

//    public static void main(String[] args) {
//        OSSClient ossClient = new OSSClient(endpoint, accessKeyId, accessKeySecret);
//        try {
//
//            // 判断Bucket是否存在。
//            if (ossClient.doesBucketExist(bucketName)) {
//                logger.info("您已经创建Bucket：" + bucketName + "。");
//            } else {
//                logger.info("您的Bucket不存在，创建Bucket：" + bucketName + "。");
//                // 创建Bucket
//                ossClient.createBucket(bucketName);
//            }
//
//            // 查看Bucket信息。
//            BucketInfo info = ossClient.getBucketInfo(bucketName);
//            logger.info("Bucket " + bucketName + "的信息如下：");
//            logger.info("\t数据中心：" + info.getBucket().getLocation());
//            logger.info("\t创建时间：" + info.getBucket().getCreationDate());
//            logger.info("\t用户标志：" + info.getBucket().getOwner());
//
//            // 把字符串存入OSS指定的bucket下的文件夹中，Object的名称为firstKey。
//            // InputStream is = new ByteArrayInputStream("Hello OSS".getBytes());
//
//            // 文件流方式存入OSS指定的bucket下
//            // InputStream inputStream = new FileInputStream("/Users/liuenyou/Desktop/备案信息/hyd.png");
//
//            // 文件形式存入OSS指定的bucket.
//            fle file = new fle("/Users/liuenyou/Desktop/备案信息/hyd.png");
//
//
//            // 自定义名称 获取当前时间 重命名 以防上传相同文件 名称冲突
//            String trueFileName = String.valueOf(System.currentTimeMillis()) + file.getName();
//
//            ossClient.putObject(bucketName, imagesKey+trueFileName, file);
//            logger.info("Object：" + imagesKey+"test.txt" + "存入OSS成功。");
//
//            // 查看Bucket中的Object。
//            ObjectListing objectListing = ossClient.listObjects(bucketName);
//            List<OSSObjectSummary> objectSummary = objectListing.getObjectSummaries();
//            logger.info("您有以下Object：");
//            for (OSSObjectSummary object : objectSummary) {
//                logger.info("\t" + object.getKey());
//            }
//        } catch (OSSException oe) {
//            oe.printStackTrace();
//        } catch (ClientException ce) {
//            ce.printStackTrace();
//        } catch (Exception e) {
//            e.printStackTrace();
//        } finally {
//            ossClient.shutdown();
//        }
//        logger.info("Completed");
//    }

}
