package com.learning.common.utils;

import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

/**
 * <p>
 *
 *    读取 工具类
 *
 * @description: </p>
 * @author: Kevin
 * @since: Created in 下午8:08 2018/3/31
 */
public class PropertiesUtil {
	private static PropertiesUtil util = null;
	private static Map<String,Properties> props = null;
	private PropertiesUtil(){
		
	}
	
	public static PropertiesUtil getInstance() {
		if(util==null) {
			props = new HashMap<String, Properties>();
			util = new PropertiesUtil();
		}
		return util;
	}
	
	public Properties load(String name) {
		if(props.get(name)!=null) {
			return props.get(name);
		} else {
			Properties prop = new Properties();
			try {
				prop.load(new InputStreamReader(Thread.currentThread().getContextClassLoader().getResourceAsStream(name + ".properties"),"UTF-8"));
				props.put(name, prop);
				return prop;
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return null;
	}
	
	public Properties loadSocket() {
		Properties properties = this.load("SocketCommand");
		return properties;
	}
	public Properties loadLanguage() {
		Properties properties = this.load("Language-zh_CN");
		return properties;
	}
	
	public Properties loadAccountConfig() {
		Properties properties = this.load("AccountServerConfig");
		return properties;
	}
	public Properties AppBackUrl() {
		Properties properties = this.load("AppBackUrl");
		return properties;
	}
	/**
	 * 
	 * @param propertiesnName 属性文件名
	 * @return
	 */
	public Properties getPropertiesByName(String propertiesnName) {
		Properties properties = this.load(propertiesnName);
		return properties;
	}
	public static void main(String[] args) {
		System.out.println(PropertiesUtil.class.getResourceAsStream(""));
	}
}
