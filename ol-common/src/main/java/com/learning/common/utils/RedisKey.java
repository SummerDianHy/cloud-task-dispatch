package com.learning.common.utils;

/**
 * <p>
 *     redis key 统一定义
 *
 * @description: </p>
 * @author: Kevin
 * @since: Created in 下午5:03 2018/4/6
 */
public class RedisKey {

    /**
     * <p>
     *     所有的redis操作 key统一在RedisKey管理
     * </p>
     * 1. key按照业务编号切割  如: 用户模块 (AngLai-User)
     * 2. key规则由 当前业务线统一ID 业务逻辑需要的key 以及时间戳定义
     *      如:(AngLai-User-token-12343958393) (忽略,暂时不用 无效)
     *
     * 3. 业务线从1000 开始 如: 用户模块 1000; 课程模块 2000; 依此类推 (忽略,暂时不用 无效)
     */

    /**************** 业务线 (user) ****************/
    public static final String TOKEN_KEY = "AngLai-User-Token";

    public static final String MOBILE_KEY = "AngLai-User-Mobile";

    public static final String ONLINE_USER = "online:";

    public static final String USER_TOKEN = "token:";






    /**************** 系统专用 *******************/
    public static String getSysConfigKey(String key){
        return "sys:config:" + key;
    }

    public static String getShiroSessionKey(String key){
        return "sessionid:" + key;
    }

}
