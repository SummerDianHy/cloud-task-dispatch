package com.learning.common.validator;

import com.learning.common.exception.RRException;
import org.apache.commons.lang.StringUtils;
/**
 * <p>
 *
 *    数据校验
 *
 * @description: </p>
 * @author: Kevin
 * @since: Created in 下午8:08 2018/3/31
 */
public abstract class Assert {

    public static void isBlank(String str, String message) {
        if (StringUtils.isBlank(str)) {
            throw new RRException(message);
        }
    }

    public static void isNull(Object object, String message) {
        if (object == null) {
            throw new RRException(message);
        }
    }
}
