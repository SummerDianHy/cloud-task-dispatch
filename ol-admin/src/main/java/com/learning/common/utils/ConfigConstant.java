package com.learning.common.utils;


/**
 * <p>
 *
 *    系统参数相关key
 *
 * @description: </p>
 * @author: Kevin
 * @since: Created in 下午8:08 2018/3/31
 */
public class ConfigConstant {
    /**
     * 云存储配置KEY
     */
    public final static String CLOUD_STORAGE_CONFIG_KEY = "CLOUD_STORAGE_CONFIG_KEY";
}
