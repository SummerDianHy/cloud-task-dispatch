package com.learning.common.utils;

import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Type;
import java.util.*;

/**
 * <p>
 *  JSON 工具类
 * </p>
 *
 * @author Kevin
 * @since 2018-3-17
 */
@SuppressWarnings("unchecked")
public class JSONUtil {

    public static final Logger logger = LoggerFactory.getLogger(JSONUtil.class);

    /**
     * 把对象转换为JSON字符
     */
    public static String toJson(Object object) {
        String jsonString = null;
        if (object != null) {
            if (object.getClass().isArray() || object instanceof List || object instanceof Collection || object instanceof Set) {
                jsonString = JSONArray.fromObject(object).toString();
            } else {
                jsonString = JSONObject.fromObject(object).toString();
            }
        }
        return jsonString == null ? "{}" : jsonString;
    }

    /**
     * 从一个JSON 对象字符格式中得到一个java对象
     */
    @SuppressWarnings("rawtypes")
    public static Object fromJson(String jsonString, Class clazz) {
        JSONObject jsonObject = null;
        try {
            jsonObject = JSONObject.fromObject(jsonString);
        } catch (Exception e) {
            LogUtil.error("转换异常", e);
        }
        return JSONObject.toBean(jsonObject, clazz);
    }

    /**
     * 从一个JSON数组得到��java对象数组
     */
    @SuppressWarnings("rawtypes")
    public static Object[] fromJsonArray(String jsonString, Class clazz) {
        JSONArray array = JSONArray.fromObject(jsonString);
        Object[] obj = new Object[array.size()];
        for (int i = 0; i < array.size(); i++) {
            JSONObject jsonObject = array.getJSONObject(i);
            obj[i] = JSONObject.toBean(jsonObject, clazz);
        }
        return obj;
    }

    /**
     * 从一个JSON数组得到java对象集合
     *
     * @param jsonString
     * @param clazz
     * @return
     */
    @SuppressWarnings({"rawtypes"})
    public static List fromJsonList(String jsonString, Class clazz) {
        JSONArray array = JSONArray.fromObject(jsonString);
        List list = new ArrayList();
        for (Iterator iter = array.iterator(); iter.hasNext(); ) {
            JSONObject jsonObject = (JSONObject) iter.next();
            list.add(JSONObject.toBean(jsonObject, clazz));
        }
        return list;
    }

    /**
     * 从json HASH表达式中获取map，该map支持嵌套功能 形如：{"id" : "johncon", "name" : "小强"}
     * 注意commons-collections版本，必须包含org.apache.commons.collections.map.MultiKeyMap
     *
     * @param jsonString
     * @return
     */
    @SuppressWarnings("rawtypes")
    public static Map getMapFromJson(String jsonString) {
        JSONObject jsonObject = JSONObject.fromObject(jsonString);
        Map map = new HashMap();
        for (Iterator iter = jsonObject.keys(); iter.hasNext(); ) {
            String key = (String) iter.next();
            map.put(key, jsonObject.get(key));
        }
        return map;
    }

    public static <T> T fromGson(String jsonString, Class<T> cls) {
        T t = null;
        try {
            Gson gson = new Gson();
            t = gson.fromJson(jsonString, cls);
        } catch (Exception e) {
            logger.error("json信息出错 : ", e);
        }
        return t;
    }


    public static <T> T fromGson(String jsonString, Type type) {
        T t = null;
        try {
            Gson gson = new Gson();
            t = gson.fromJson(jsonString, type);
        } catch (Exception e) {
            logger.error("json信息出错 : ", e);
        }
        return t;
    }

    public static <T> List<T> fromGsonList(String jsonString, Class<T> clazz) {
        Gson gson = new Gson();
        List<T> list = new ArrayList<T>();
        try {
            JsonArray array = new JsonParser().parse(jsonString).getAsJsonArray();
            for (final JsonElement elem : array) {
                list.add(gson.fromJson(elem, clazz));
            }
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
        return list;
    }

    public static List<Map<String, Object>> listKeyMaps(String jsonString) {
        List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
        try {
            Gson gson = new Gson();
            list = gson.fromJson(jsonString, new TypeToken<List<Map<String, Object>>>() {
            }.getType());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    public static Map<String, Object> getMap(String jsonString) {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            Gson gson = new Gson();
            map = gson.fromJson(jsonString, new TypeToken<Map<String, Object>>() {
            }.getType());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return map;
    }

    /**
     * 从json数组中得到相应java数组 json形如：["123", "456"]
     */
    public static Object[] getObjectArrayFromJson(String jsonString) {
        JSONArray jsonArray = JSONArray.fromObject(jsonString);
        return jsonArray.toArray();
    }

    public static void main(String[] args) throws Exception {
    /*	String x = null;
		List<Person> listt = new ArrayList<Person>();
		for (int i = 0; i <= 100; i++) {
			listt.add(new Person("张三" + i, i));
		}
		try {
			x = getJSONString(listt);
		} catch (Exception e) {
			LogUtil.error("", e);
		}
		System.out.println(x);
		listt = (List<Person>) getDTOList(x, Person.class);
		System.out.println(listt.size());*/
    }

    public static void testJsonConfig() throws Exception {
        // Person person = new Person("张三", 18);
        // Dog dog = new Dog("张三", "黑背");
        // Hourse hourse = new Hourse("唐宁08, person, dog);
        // String str = getJSONString(hourse);
        // System.out.println(str);
        // Map x = new HashMap();
        // x.put("person", Person.class);
        // x.put("Dog", Dog.class);
        // getDTO(str, Hourse.class);
    }

    public static class Hourse {
        private String adress;
        private Person person;
        private Dog dog;

        public Hourse() {
        }

        public Hourse(String adress, Person person, Dog dog) {
            this.adress = adress;
            this.person = person;
            this.dog = dog;
        }

        public String getAdress() {
            return adress;
        }

        public void setAdress(String adress) {
            this.adress = adress;
        }

        public Person getPerson() {
            return person;
        }

        public void setPerson(Person person) {
            this.person = person;
        }

        public Dog getDog() {
            return dog;
        }

        public void setDog(Dog dog) {
            this.dog = dog;
        }
    }

    public static class Person {
        private String name;
        private int age;

        public Person() {
        }

        public Person(String name, int age) {
            this.name = name;
            this.age = age;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public int getAge() {
            return age;
        }

        public void setAge(int age) {
            this.age = age;
        }

        public String toString() {
            return "name :" + name + "___age :" + age;
        }
    }

    public static class Dog {
        private String master;
        private String variety;

        public Dog() {
        }

        public Dog(String master, String variety) {
            this.master = master;
            this.variety = variety;
        }

        public String getMaster() {
            return master;
        }

        public void setMaster(String master) {
            this.master = master;
        }

        public String getVariety() {
            return variety;
        }

        public void setVariety(String variety) {
            this.variety = variety;
        }

        public String toString() {
            return " master :" + master + " __variety" + variety;
        }
    }
}
