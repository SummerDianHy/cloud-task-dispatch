package com.learning.common.utils;

/**
 * <p>
 *
 *    redis 所有的key
 *
 * @description: </p>
 * @author: Kevin
 * @since: Created in 下午8:08 2018/3/31
 */
public class RedisKeys {

    public static String getSysConfigKey(String key){
        return "sys:config:" + key;
    }

    public static String getShiroSessionKey(String key){
        return "sessionid:" + key;
    }
}
