package com.learning.common.annotation;

import java.lang.annotation.*;

/**
 * <p>
 *
 *    数据过滤
 *
 * @description: </p>
 * @author: Kevin
 * @since: Created in 下午8:08 2018/3/31
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface DataFilter {

    /**  表的别名 */
    String tableAlias() default "";

    /**  用户ID */
    String userId() default "user_id";
}

