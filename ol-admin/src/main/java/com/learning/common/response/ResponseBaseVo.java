package com.learning.common.response;


import com.learning.common.utils.JSONUtil;

import java.io.Serializable;

/**
 * <p>
 *  消息返回 工具类
 * </p>
 *
 * @author Kevin
 * @since 2018-3-17
 */
public class ResponseBaseVo implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 4992866296675736795L;
    /**
     * 0成功，1失败,2没有登录
     **/
    protected Integer code;
    /**
     * 消息
     **/
    protected String message;

    public ResponseBaseVo() {
    }

    public ResponseBaseVo(Integer code, String message) {
        this.code = code;
        this.message = message;
    }

    public static ResponseBaseVo success() {
        return new ResponseBaseVo(ResponseConstant.SUCCESS_CODE, ResponseConstant.SUCCESS_MSG);
    }

    public static ResponseBaseVo erro() {
        return erro(ResponseConstant.ERROR_MSG);
    }

    public static ResponseBaseVo erro(String message) {
        return new ResponseBaseVo(ResponseConstant.ERROR_CODE, message);
    }

    public static ResponseBaseVo loginTimeOut() {
        return new ResponseBaseVo(ResponseConstant.SESSION_DISABLED, ResponseConstant.BUSINESS_ERROR_MSG);
    }

    public static ResponseBaseVo logicalError(String msg) {
        return new ResponseBaseVo(ResponseConstant.BUSINESS_ERROR, msg);
    }

    public static ResponseBaseVo logicalError() {
        return new ResponseBaseVo(ResponseConstant.BUSINESS_ERROR, ResponseConstant.BUSINESS_ERROR_MSG);
    }

    public static ResponseBaseVo systemError(String msg) {
        return new ResponseBaseVo(ResponseConstant.SYSTEM_ERROR, ResponseConstant.SYSTEM_ERROR_MSG);
    }

    public static ResponseBaseVo accessError(String msg) {
        return new ResponseBaseVo(ResponseConstant.ACCESS_ERROR, ResponseConstant.ACCESS_MSG);
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return JSONUtil.toJson(this);
    }
}
