package com.learning.common.response;

import java.io.Serializable;

/**
 * <p>
 *   返回JSON
 * </p>
 *
 * @author Kevin
 * @since 2018-3-17
 */
public class ResponseDataVo<T> extends ResponseBaseVo implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8389534598865802502L;

	private T data;

	public ResponseDataVo() {

	}

	public ResponseDataVo(T data, Integer code, String message) {
		super(code, message);
		this.data = data;
	}
	
	public static <T> ResponseDataVo<T> success(T t) {
		return new ResponseDataVo<T>(t, ResponseConstant.SUCCESS_CODE, ResponseConstant.SUCCESS_MSG);
	}

	public static <T> ResponseDataVo<T> error() {
		return error(ResponseConstant.ERROR_MSG);
	}

	public static <T> ResponseDataVo<T> error(String message) {
		return new ResponseDataVo<T>(null, ResponseConstant.ERROR_CODE, message);
	}

	public static <T> ResponseDataVo<T> logicError(String message) {
		return new ResponseDataVo<T>(null, ResponseConstant.BUSINESS_ERROR, message);
	}

	public static <T> ResponseDataVo<T> sysError(String message) {
		return new ResponseDataVo<T>(null, ResponseConstant.SYSTEM_ERROR, message);
	}
	
	public T getData() {
		return data;
	}

	public void setData(T data) {
		this.data = data;
	}
	
}
