package com.learning.util;

import com.alibaba.fastjson.JSON;
import com.aliyuncs.DefaultAcsClient;
import com.aliyuncs.IAcsClient;
import com.aliyuncs.dysmsapi.model.v20170525.SendSmsRequest;
import com.aliyuncs.dysmsapi.model.v20170525.SendSmsResponse;
import com.aliyuncs.exceptions.ClientException;
import com.aliyuncs.profile.DefaultProfile;
import com.aliyuncs.profile.IClientProfile;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * <p>
 * Aliyun SMS 发起请求工具类
 * </p>
 *
 * @author Kevin
 * @since 2018-3-14
 */
@Component
public class AliSMSUtil {

    private static final Logger logger = LoggerFactory.getLogger(AliSMSUtil.class);

    //产品名称:云通信短信API产品
    static final String product = "Dysmsapi";
    //产品域名
    static final String domain = "dysmsapi.aliyuncs.com";

    // 短信秘钥
    static final String accessKeyId = "LTAIb7f2Mch7hSpx";
    static final String accessKeySecret = "5nQp7XcqBHU7xYqsBpLTqYQigLdvJ4";

    public SendSmsResponse sendSms(String phone, String templateCode,String templateParam) throws ClientException {

        //可自助调整超时时间
        System.setProperty("sun.net.client.defaultConnectTimeout", "10000");
        System.setProperty("sun.net.client.defaultReadTimeout", "10000");

        //初始化acsClient,暂不支持region化
        IClientProfile profile = DefaultProfile.getProfile("cn-hangzhou", accessKeyId, accessKeySecret);
        DefaultProfile.addEndpoint("cn-hangzhou", "cn-hangzhou", product, domain);
        IAcsClient acsClient = new DefaultAcsClient(profile);

        //组装请求对象
        SendSmsRequest request = new SendSmsRequest();
        //必填:待发送手机号
        request.setPhoneNumbers(phone);
        //必填:短信模板
        request.setTemplateCode(templateCode);
        //必填:短信签名
        request.setSignName("小昂云 - 调度平台教育");
        //可选:模板中的变量替换JSON串,如模板内容为"亲爱的${name},您的验证码为${code}"时,此处的值为
        request.setTemplateParam(templateParam);
        //可选:outId为提供给业务方扩展字段,最终在短信回执消息中将此值带回给调用者
        request.setOutId("yourOutId");
        try {
            SendSmsResponse sendSmsResponse = acsClient.getAcsResponse(request);
            logger.info("打印阿里云回调信息:{}", JSON.toJSON(sendSmsResponse));
            return sendSmsResponse;
        } catch (ClientException ce) {
            ce.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

}
