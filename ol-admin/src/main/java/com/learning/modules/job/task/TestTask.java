package com.learning.modules.job.task;

import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;
import com.learning.common.utils.RedisUtils;
import com.learning.modules.sys.entity.system.SysUserEntity;
import com.learning.modules.sys.service.system.SysUserService;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import org.springframework.beans.factory.annotation.Autowired;

import javax.xml.stream.events.Comment;
import java.util.List;

/**
 * <p>
 *
 * 测试定时任务(演示Demo，可删除)
 *
 * testTask为spring bean的名称
 *
 * @description: </p>
 * @author: Kevin
 * @since: Created in 下午8:08 2018/3/31
 */
@Component("testTask")
public class TestTask {
	private Logger logger = LoggerFactory.getLogger(getClass());
	
	@Autowired
	private SysUserService sysUserService;
	@Autowired
	private RedisUtils redisUtils;

	@Autowired
	private RedisTemplate<String, String> redisTemplate;

	public void test(String params){
		String redisstr = redisTemplate.opsForList().range("smsLogList", 0, redisTemplate.opsForList().size("smsLogList")).toString();
		List<Comment> comments = new Gson().fromJson(redisstr,new TypeToken<List<Comment>>(){}.getType());
		System.out.println(redisstr);
		
	}
	
	
	public void test2(){
		logger.info("我是不带参数的test2方法，正在被执行");
	}
}
