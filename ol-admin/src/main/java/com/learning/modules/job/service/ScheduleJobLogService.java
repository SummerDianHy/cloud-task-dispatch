package com.learning.modules.job.service;

import com.baomidou.mybatisplus.service.IService;
import com.learning.common.utils.PageUtils;
import com.learning.modules.job.entity.ScheduleJobLogEntity;

import java.util.Map;


/**
 * <p>
 *
 *    定时任务日志
 *
 * @description: </p>
 * @author: Kevin
 * @since: Created in 下午8:08 2018/3/31
 */
public interface ScheduleJobLogService extends IService<ScheduleJobLogEntity> {

	PageUtils queryPage(Map<String, Object> params);
	
}
