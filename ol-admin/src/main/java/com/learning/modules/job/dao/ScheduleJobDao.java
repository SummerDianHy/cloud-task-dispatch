package com.learning.modules.job.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.learning.modules.job.entity.ScheduleJobEntity;

import java.util.Map;


/**
 * <p>
 *
 *    定时任务
 *
 * @description: </p>
 * @author: Kevin
 * @since: Created in 下午8:08 2018/3/31
 */
public interface ScheduleJobDao extends BaseMapper<ScheduleJobEntity> {
	
	/**
	 * 批量更新状态
	 */
	int updateBatch(Map<String, Object> map);
}
