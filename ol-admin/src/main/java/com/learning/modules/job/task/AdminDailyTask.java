package com.learning.modules.job.task;

import com.learning.modules.sys.service.base.IBaseService;
import com.learning.modules.sys.service.statistics.IStatisticsAdminDailyService;
import com.learning.modules.sys.service.statistics.IStatisticsCourseService;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * create by: JunAo.Yuan
 * description: 平台每日数据统计
 * create time: 14:58 2018/10/19
 * 
 */
@Component("adminDailyTask")
public class AdminDailyTask {

    private static final Logger logger = LoggerFactory.getLogger(AdminDailyTask.class);

    // 统计数据 查询
    @Autowired
    private IBaseService baseService;

    // 统计
    @Autowired
    private IStatisticsAdminDailyService adminService;

    /*// 用户登陆数
    public void loginNums() {
        //企业端
        List<HashMap<String, Object>> list = baseService.countLoginNumsDaily();
        checkListNull(list);
    }

    // 新增课程数
    public void courseCreate() {
        //企业端
        List<HashMap<String, Object>> list = baseService.courseCreateNumDaily();
        checkListNull(list);
    }

    // 课程学习人数统计
    public void countStudyNums() {
        //企业端
        List<HashMap<String, Object>> list = baseService.countStudyNumsDaily();
        checkListNull(list);
    }

    // 课程完成数统计
    public void countCourseFinished() {
        //企业端
        List<HashMap<String, Object>> list = baseService.countCourseFinishedDaily();
        checkListNull(list);
    }*/



    private void checkListNull(List<HashMap<String, Object>> mapInfo) {
        if (null != mapInfo) {
            for (HashMap<String, Object> map : mapInfo) {
                Iterator iterator = map.entrySet().iterator();
                while (iterator.hasNext()) {
                    Map.Entry entry = (Map.Entry) iterator.next();
                    String key = (String) entry.getKey();
                    if (!StringUtils.isNotBlank(key)) {
                        iterator.remove();
                    }
                }
            }
            adminService.saveAdminDaily(mapInfo);
        }
    }

}



