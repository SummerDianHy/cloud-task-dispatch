package com.learning.modules.job.task;

import com.learning.modules.sys.service.base.IBaseService;
import com.learning.modules.sys.service.base.IPlatFormBaseService;
import com.learning.modules.sys.service.statistics.IStatisticsCourseDailyService;
import com.learning.modules.sys.service.statistics.IStatisticsUserDailyService;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * create by: JunAo.Yuan
 * description:用户数据统计-每日 - 定时任务
 * create time: 16:51 2018/10/20
 *
 */
@Component("userDailyTask")
public class UserDailyTask {

    private static final Logger logger = LoggerFactory.getLogger(UserDailyTask.class);

    private static final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
    // 统计数据 查询企业端
    @Autowired
    private IBaseService baseService;
    // 统计数据 查询平台端
    @Autowired
    private IPlatFormBaseService iPlatFormBaseService;


    // 课程统计
    @Autowired
    private IStatisticsUserDailyService userService;

    // 学习时长
    public void learningTimeDaily(String param) {
        Date date = new Date();
        if (!param.equals("n")) {
            try {
                date = sdf.parse(param);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        //企业端
        List<HashMap<String, Object>> list = baseService.learningTimeDaily(date);
        checkListNull(list);
    }

    // 课程数
    public void courseNumsDaily(String param) {
        Date date = new Date();
        if (!param.equals("n")) {
            try {
                date = sdf.parse(param);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        //企业端
        List<HashMap<String, Object>> list = baseService.courseNumsDaily(date);
        checkListNull(list);
        //平台端
//        List<String> idList = baseService.getAllNotLocalCourseId();
//        List<HashMap<String, Object>> list1 = iPlatFormBaseService.favoriteNumDay(idList);
//        checkListNull(list1);
    }

    // 课程学习完成数
    public void courseFinishedUserDaily(String param) {
        Date date = new Date();
        if (!param.equals("n")) {
            try {
                date = sdf.parse(param);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        //企业端
        List<HashMap<String, Object>> list = baseService.courseFinishedUserDaily(date);
        checkListNull(list);

    }
    // 笔记数量
    public void noteNumsDaily(String param) {
        Date date = new Date();
        if (!param.equals("n")) {
            try {
                date = sdf.parse(param);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        //企业端
        List<HashMap<String, Object>> list = baseService.noteNumsDaily(date);
        checkListNull(list);
        //平台端
//        List<String> idList = baseService.getAllNotLocalCourseId();
//        List<HashMap<String, Object>> list1 = iPlatFormBaseService.studentNumDay(idList);
//        checkListNull(list1);
    }

    // 课程公开笔记数统计
    public void noteOpenNumsDaily(String param) {
        Date date = new Date();
        if (!param.equals("n")) {
            try {
                date = sdf.parse(param);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        //企业端
        List<HashMap<String, Object>> list = baseService.noteOpenNumsDaily(date);
        checkListNull(list);
        //平台端
//        List<String> idList = baseService.getAllNotLocalCourseId();
//        List<HashMap<String, Object>> list1 = iPlatFormBaseService.finishStudyNumDay(idList);
//        checkListNull(list1);
    }

    // 课程评论数统计
    public void topicNumsDaily(String param) {
        Date date = new Date();
        if (!param.equals("n")) {
            try {
                date = sdf.parse(param);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        //企业端
        List<HashMap<String, Object>> list = baseService.topicNumsDaily(date);
        checkListNull(list);
        //平台端
//        List<String> idList = baseService.getAllNotLocalCourseId();
//        List<HashMap<String, Object>> list1 = iPlatFormBaseService.viewNumDay(idList);
//        checkListNull(list1);
    }

    // 课程评论回复数统计
    public void topicReplyNumsDaily(String param) {
        Date date = new Date();
        if (!param.equals("n")) {
            try {
                date = sdf.parse(param);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        //企业端
        List<HashMap<String, Object>> list = baseService.topicReplyNumsDaily(date);
        checkListNull(list);
        //平台端
//        List<String> idList = baseService.getAllNotLocalCourseId();
//        List<HashMap<String, Object>> list1 = iPlatFormBaseService.noteNumDay(idList);
//        checkListNull(list1);
    }

    // 课程收藏数统计
    public void courseCollectionDaily(String param) {
        Date date = new Date();
        if (!param.equals("n")) {
            try {
                date = sdf.parse(param);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        //企业端
        List<HashMap<String, Object>> list = baseService.courseCollectionDaily(date);
        checkListNull(list);
    }

    // 课程评价次数
    public void courseJudgeDaily(String param) {
        Date date = new Date();
        if (!param.equals("n")) {
            try {
                date = sdf.parse(param);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        //企业端
        List<HashMap<String, Object>> list = baseService.courseJudgeDaily(date);
        checkListNull(list);
    }

    // 私信数统计
    public void privateMessageDaily(String param) {
        Date date = new Date();
        if (!param.equals("n")) {
            try {
                date = sdf.parse(param);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        //企业端
        List<HashMap<String, Object>> list = baseService.privateMessageDaily(date);
        checkListNull(list);
        //平台端
//        List<String> idList = baseService.getAllNotLocalCourseId();
//        List<HashMap<String, Object>> list1 = iPlatFormBaseService.courseJudgeNumDay(idList);
//        checkListNull(list1);
    }

    private void checkListNull(List<HashMap<String, Object>> mapInfo) {
        if (null != mapInfo) {
            for (HashMap<String, Object> map : mapInfo) {
                Iterator iterator = map.entrySet().iterator();
                while (iterator.hasNext()) {
                    Map.Entry entry = (Map.Entry) iterator.next();
                    String key = (String) entry.getKey();
                    if (!StringUtils.isNotBlank(key)) {
                        iterator.remove();
                    }
                }
            }
            userService.saveUserDaily(mapInfo);
        }
    }

}



