package com.learning.modules.job.task;

import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;
import com.learning.modules.sys.dao.statistics.MerchantSMSLogFreeDao;
import com.learning.modules.sys.entity.statistics.MerchantSmsLog;
import com.learning.modules.sys.service.base.IFreeService;
import com.learning.modules.sys.service.statistics.IMerchantCdnLogService;
import com.learning.modules.sys.service.statistics.IMerchantSmsLogService;
import com.learning.modules.sys.service.statistics.IStatisticsFreeService;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import java.rmi.ServerError;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * create by: JunAo.Yuan
 * description: cdn+sms - 定时任务
 * create time: 16:52 2018/10/20
 */
@Component("cdnAndSmsTask")
public class CdnAndSmsTask {

    private static final Logger logger = LoggerFactory.getLogger(CdnAndSmsTask.class);

    @Autowired
    private IMerchantSmsLogService iMerchantSmsLogService;

    @Autowired
    private RedisTemplate<String, String> redisTemplate;
    // 统计数据
    @Autowired
    private IFreeService iFreeService;

    // 费用结算
    @Autowired
    private IStatisticsFreeService freeService;

    // 费用结算
    @Autowired
    private IMerchantCdnLogService cdnLogService;

    //每天23:50:01开始统计商户当天CDN费用记录 date=‘yyyy-MM-dd 16:00:00’
    public void getMerchantCDNFreeRecord() {
        cdnLogService.saveCDNDaily(null);
    }

    //统计CDN费用日志
    public void getCDNFreeRecord() {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        //判断是否本星期的数据都统计全
        Map<String,String> weekDate = getWeekDayStr();
        List<HashMap<String, Date>> mapInfo = iFreeService.getCDNFreeRecordGroupByDay();
        for (String values : weekDate.values()) {
            boolean flag = false;
            for (HashMap<String, Date> map : mapInfo) {
                for (Date value : map.values()){
                    Date a = null;
                    try {
                        a = format.parse(values);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    if (a.equals(value)) {
                        flag = true;
                    }
                }
            }
            if (!flag) {
                cdnLogService.saveCDNDaily(values+" 16:00:00");
            }
        }

        List<HashMap<String, Object>> mapInfo1 = iFreeService.getCDNFreeRecord();
        checkListNull(mapInfo1);
    }

    //统计SMS费用日志
    public void getSMSFreeRecord() {
        try {
            String redisStr = redisTemplate.opsForList().range("smsLogList", 0, redisTemplate.opsForList().size("smsLogList")).toString();
            List<MerchantSmsLog> logs = new Gson().fromJson(redisStr,new TypeToken<List<MerchantSmsLog>>(){}.getType());
            if (StringUtils.isNotEmpty(redisStr)) {
                iMerchantSmsLogService.insertBatchs(logs);
            }
            redisTemplate.delete("smsLogList");
        } catch (Exception e) {
            e.printStackTrace();
        }
        List<HashMap<String, Object>> mapInfo = iFreeService.getSMSFreeRecord();
        checkListNullTwo(mapInfo);
    }

    private void checkListNull(List<HashMap<String, Object>> mapInfo) {
        if (null != mapInfo) {
            for (HashMap<String, Object> map : mapInfo) {
                Iterator iterator = map.entrySet().iterator();
                while (iterator.hasNext()) {
                    Map.Entry entry = (Map.Entry) iterator.next();
                    String key = (String) entry.getKey();
                    if (!StringUtils.isNotBlank(key)) {
                        iterator.remove();
                    }
                }
            }
            freeService.saveFree(mapInfo);
        }
    }
    private void checkListNullTwo(List<HashMap<String, Object>> mapInfo) {
        if (null != mapInfo) {
            for (HashMap<String, Object> map : mapInfo) {
                Iterator iterator = map.entrySet().iterator();
                while (iterator.hasNext()) {
                    Map.Entry entry = (Map.Entry) iterator.next();
                    String key = (String) entry.getKey();
                    if (!StringUtils.isNotBlank(key)) {
                        iterator.remove();
                    }
                }
            }
            freeService.saveFreeTwo(mapInfo);
        }
    }

    private Map<String,String> getWeekDayStr(){
        Map<String, String> date = new HashMap<>();
        // 日期格式转换
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        // 当前日期
        Calendar instance = Calendar.getInstance();
        // 调整到上周
//        instance.add(Calendar.WEDNESDAY, -1);
        // 调整到上周1
        instance.set(Calendar.DAY_OF_WEEK, 2);
        //循环打印
        for (int i = 1; i <= 7; i++) {
            date.put("a"+i,format.format(instance.getTime()));
            System.out.println("星期" + i + ":" + format.format(instance.getTime()));
            instance.add(Calendar.DAY_OF_WEEK, 1);
        }
        return date;
    }


}



