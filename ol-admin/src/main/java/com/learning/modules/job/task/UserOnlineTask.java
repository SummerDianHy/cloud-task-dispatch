package com.learning.modules.job.task;

import com.learning.modules.sys.entity.userOnline.UserOnline;
import com.learning.modules.sys.service.base.IBaseService;
import com.learning.modules.sys.service.base.IPlatFormBaseService;
import com.learning.modules.sys.service.statistics.IStatisticsUserService;
import com.learning.modules.sys.service.statistics.IUserOnlineService;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * create by: JunAo.Yuan
 * description:用户在线 - 定时任务
 * create time: 16:51 2018/11/20
 *
 */
@Component("userOnlineTask")
public class UserOnlineTask {

    private static final Logger logger = LoggerFactory.getLogger(UserOnlineTask.class);

    // 统计数据 查询企业端
    @Autowired
    private IBaseService baseService;

    // 统计数据 查询平台端
    @Autowired
    private IPlatFormBaseService platFormBaseService;

    // 用户在线统计
    @Autowired
    private IUserOnlineService iUserOnlineService;

    // 更改用户在线状态
    public void updateOnlineUser() {
        List<UserOnline> list = baseService.selectOnlineUserList();
        iUserOnlineService.updateUserOnline(list);
    }

    // 更改商户到期状态
    public void updateMerchantStatus() {
        //企业端
        List<HashMap<String, Object>> list = baseService.selectEnableMerchantList();
        iUserOnlineService.updateMerchantStatus(list);
        //平台端
        List<HashMap<String, Object>> list1 = platFormBaseService.selectEnableMerchantList();
        iUserOnlineService.updateMerchantStatusTwo(list1);
    }



}



