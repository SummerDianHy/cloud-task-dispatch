package com.learning.modules.job.task;

import com.learning.modules.sys.service.base.IBaseService;
import com.learning.modules.sys.service.base.IPlatFormBaseService;
import com.learning.modules.sys.service.statistics.IStatisticsCourseDailyService;
import com.learning.modules.sys.service.statistics.IStatisticsCourseService;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * create by: JunAo.Yuan
 * description:课程每日统计 - 定时任务
 * create time: 16:52 2018/10/20
 */
@Component("courseDailyTask")
public class CourseDailyTask {

    private static final Logger logger = LoggerFactory.getLogger(CourseDailyTask.class);

    private static final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
    // 统计数据 查询企业端
    @Autowired
    private IBaseService baseService;
    // 统计数据 查询平台端
    @Autowired
    private IPlatFormBaseService iPlatFormBaseService;


    // 课程统计
    @Autowired
    private IStatisticsCourseDailyService courseService;

    // 处理课程销售统计
    public void courseIncomeDay(String param) {
        Date date = new Date();
        if (!param.equals("n")) {
            try {
                date = sdf.parse(param);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        //企业端
        List<HashMap<String, Object>> list = baseService.countCourseIncomeDay(date);
        checkListNull(list);
    }

    // 收藏人数统计
    public void favoriteNumDay(String param) {
        Date date = new Date();
        if (!param.equals("n")) {
            try {
                date = sdf.parse(param);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        //企业端
        List<HashMap<String, Object>> list = baseService.favoriteNumDay(date);
        checkListNull(list);
        //平台端
        /*List<String> idList = baseService.getAllNotLocalCourseId();
        List<HashMap<String, Object>> list1 = iPlatFormBaseService.favoriteNumDay(idList);
        checkListNull(list1);*/
    }

    // 课程学员数统计
    public void studentNumDay(String param) {
        Date date = new Date();
        if (!param.equals("n")) {
            try {
                date = sdf.parse(param);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        //企业端
        List<HashMap<String, Object>> list = baseService.studentNumDay(date);
        checkListNull(list);
        //平台端
        /*List<String> idList = baseService.getAllNotLocalCourseId();
        List<HashMap<String, Object>> list1 = iPlatFormBaseService.studentNumDay(idList);
        checkListNull(list1);*/
    }

    // 课程学习人数统计
    public void studyNumDay(String param) {
        Date date = new Date();
        if (!param.equals("n")) {
            try {
                date = sdf.parse(param);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        //企业端
        List<HashMap<String, Object>> list = baseService.studyNumDay(date);
        checkListNull(list);
    }

    // 课程完成人数统计
    public void finishStudyNumDay(String param) {
        Date date = new Date();
        if (!param.equals("n")) {
            try {
                date = sdf.parse(param);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        //企业端
        List<HashMap<String, Object>> list = baseService.finishStudyNumDay(date);
        checkListNull(list);
        //平台端
        /*List<String> idList = baseService.getAllNotLocalCourseId();
        List<HashMap<String, Object>> list1 = iPlatFormBaseService.finishStudyNumDay(idList);
        checkListNull(list1);*/
    }

    // 课程浏览数统计
    public void viewNumDay(String param) {
        Date date = new Date();
        if (!param.equals("n")) {
            try {
                date = sdf.parse(param);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        //企业端
        List<HashMap<String, Object>> list = baseService.viewNumDay(date);
        checkListNull(list);
        //平台端
        /*List<String> idList = baseService.getAllNotLocalCourseId();
        List<HashMap<String, Object>> list1 = iPlatFormBaseService.viewNumDay(idList);
        checkListNull(list1);*/
    }

    // 课程笔记统计
    public void noteNumDay(String param) {
        Date date = new Date();
        if (!param.equals("n")) {
            try {
                date = sdf.parse(param);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        //企业端
        List<HashMap<String, Object>> list = baseService.noteNumDay(date);
        checkListNull(list);
        //平台端
        /*List<String> idList = baseService.getAllNotLocalCourseId();
        List<HashMap<String, Object>> list1 = iPlatFormBaseService.noteNumDay(idList);
        checkListNull(list1);*/
    }

    // 课程评论分数
    public void courseRatingDay(String param) {
        Date date = new Date();
        if (!param.equals("n")) {
            try {
                date = sdf.parse(param);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        //企业端
        List<HashMap<String, Object>> list = baseService.courseRatingDay(date);
        checkListNull(list);
    }

    // 课程好评次数
    public void judgeGoodNumDay(String param) {
        Date date = new Date();
        if (!param.equals("n")) {
            try {
                date = sdf.parse(param);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        //企业端
        List<HashMap<String, Object>> list = baseService.judgeGoodNumDay(date);
        checkListNull(list);
    }

    // 课程评论次数
    public void courseJudgeNumDay(String param) {
        Date date = new Date();
        if (!param.equals("n")) {
            try {
                date = sdf.parse(param);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        //企业端
        List<HashMap<String, Object>> list = baseService.courseJudgeNumDay(date);
        checkListNull(list);
        //平台端
        /*List<String> idList = baseService.getAllNotLocalCourseId();
        List<HashMap<String, Object>> list1 = iPlatFormBaseService.courseJudgeNumDay(idList);
        checkListNull(list1);*/
    }

    // 课程话题数量
    public void courseTopicNumDay(String param) {
        Date date = new Date();
        if (!param.equals("n")) {
            try {
                date = sdf.parse(param);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        //企业端
        List<HashMap<String, Object>> list = baseService.courseTopicNumDay(date);
        checkListNull(list);
        //平台端
        /*List<String> idList = baseService.getAllNotLocalCourseId();
        List<HashMap<String, Object>> list1 = iPlatFormBaseService.courseTopicNumDay(idList);
        checkListNull(list1);*/
    }

    // 话题回复数量
    public void courseTopicReplyNumDay(String param) {
        Date date = new Date();
        if (!param.equals("n")) {
            try {
                date = sdf.parse(param);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        //企业端
        List<HashMap<String, Object>> list = baseService.courseTopicReplyNumDay(date);
        checkListNull(list);
        //平台端
        /*List<String> idList = baseService.getAllNotLocalCourseId();
        List<HashMap<String, Object>> list1 = iPlatFormBaseService.courseTopicReplyNumDay(idList);
        checkListNull(list1);*/
    }

    // 课程笔记公开数量
    public void courseNoteOpenNumDay(String param) {
        Date date = new Date();
        if (!param.equals("n")) {
            try {
                date = sdf.parse(param);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        //企业端
        List<HashMap<String, Object>> list = baseService.courseNoteOpenNumDay(date);
        checkListNull(list);
        //平台端
        /*List<String> idList = baseService.getAllNotLocalCourseId();
        List<HashMap<String, Object>> list1 = iPlatFormBaseService.courseNoteOpenNumDay(idList);
        checkListNull(list1);*/
    }


    private void checkListNull(List<HashMap<String, Object>> mapInfo) {
        if (null != mapInfo) {
            for (HashMap<String, Object> map : mapInfo) {
                Iterator iterator = map.entrySet().iterator();
                while (iterator.hasNext()) {
                    Map.Entry entry = (Map.Entry) iterator.next();
                    String key = (String) entry.getKey();
                    if (!StringUtils.isNotBlank(key)) {
                        iterator.remove();
                    }
                }
            }
            courseService.saveCourse(mapInfo);
        }
    }

}



