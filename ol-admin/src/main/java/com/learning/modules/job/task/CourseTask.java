package com.learning.modules.job.task;

import com.learning.modules.sys.service.base.IBaseService;
import com.learning.modules.sys.service.base.IPlatFormBaseService;
import com.learning.modules.sys.service.statistics.IStatisticsCourseService;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * create by: JunAo.Yuan
 * description:课程统计 - 定时任务
 * create time: 16:52 2018/10/20
 */
@Component("courseTask")
public class CourseTask {

    private static final Logger logger = LoggerFactory.getLogger(CourseTask.class);

    // 统计数据 查询企业端
    @Autowired
    private IBaseService baseService;
    // 统计数据 查询平台端
    @Autowired
    private IPlatFormBaseService iPlatFormBaseService;


    // 课程统计
    @Autowired
    private IStatisticsCourseService courseService;

    // 处理课程销售统计
    public void courseIncome() {
        //企业端
        List<HashMap<String, Object>> list = baseService.countCourseIncome();
        checkListNull(list);
    }

    // 收藏人数统计
    public void favoriteNum() {
        //企业端
        List<HashMap<String, Object>> list = baseService.favoriteNum();
        checkListNull(list);
        //平台端
        /*List<String> idList = baseService.getAllNotLocalCourseId();
        List<HashMap<String, Object>> list1 = iPlatFormBaseService.favoriteNum(idList);
        checkListNull(list1);*/
    }

    // 课程学员数统计
    public void studentNum() {
        //企业端
        List<HashMap<String, Object>> list = baseService.studentNum();
        checkListNull(list);
        //平台端
        /*List<String> idList = baseService.getAllNotLocalCourseId();
        List<HashMap<String, Object>> list1 = iPlatFormBaseService.studentNum(idList);
        checkListNull(list1);*/
    }

    // 课程学习人数统计
    public void studyNum() {
        //企业端
        List<HashMap<String, Object>> list = baseService.studyNum();
        checkListNull(list);
    }

    // 课程完成人数统计
    public void finishStudyNum() {
        //企业端
        List<HashMap<String, Object>> list = baseService.finishStudyNum();
        checkListNull(list);
        //平台端
        /*List<String> idList = baseService.getAllNotLocalCourseId();
        List<HashMap<String, Object>> list1 = iPlatFormBaseService.finishStudyNum(idList);
        checkListNull(list1);*/
    }

    // 课程浏览数统计
    public void viewNum() {
        //企业端
        List<HashMap<String, Object>> list = baseService.viewNum();
        checkListNull(list);
        //平台端
        /*List<String> idList = baseService.getAllNotLocalCourseId();
        List<HashMap<String, Object>> list1 = iPlatFormBaseService.viewNum(idList);
        checkListNull(list1);*/
    }

    // 课程笔记统计
    public void noteNum() {
        //企业端
        List<HashMap<String, Object>> list = baseService.noteNum();
        checkListNull(list);
        //平台端
        /*List<String> idList = baseService.getAllNotLocalCourseId();
        List<HashMap<String, Object>> list1 = iPlatFormBaseService.noteNum(idList);
        checkListNull(list1);*/
    }

    // 课程评论分数
    public void courseRating() {
        //企业端
        List<HashMap<String, Object>> list = baseService.courseRating();
        checkListNull(list);
    }

    // 课程好评次数
    public void judgeGoodNum() {
        //企业端
        List<HashMap<String, Object>> list = baseService.judgeGoodNum();
        checkListNull(list);
    }

    // 课程评论次数
    public void courseJudgeNum() {
        //企业端
        List<HashMap<String, Object>> list = baseService.courseJudgeNum();
        checkListNull(list);
        //平台端
        /*List<String> idList = baseService.getAllNotLocalCourseId();
        List<HashMap<String, Object>> list1 = iPlatFormBaseService.courseJudgeNum(idList);
        checkListNull(list1);*/
    }

    // 课程话题数量
    public void courseTopicNum() {
        //企业端
        List<HashMap<String, Object>> list = baseService.courseTopicNum();
        checkListNull(list);
        //平台端
        /*List<String> idList = baseService.getAllNotLocalCourseId();
        List<HashMap<String, Object>> list1 = iPlatFormBaseService.courseTopicNum(idList);
        checkListNull(list1);*/
    }

    // 话题回复数量
    public void courseTopicReplyNum() {
        //企业端
        List<HashMap<String, Object>> list = baseService.courseTopicReplyNum();
        checkListNull(list);
        //平台端
        /*List<String> idList = baseService.getAllNotLocalCourseId();
        List<HashMap<String, Object>> list1 = iPlatFormBaseService.courseTopicReplyNum(idList);
        checkListNull(list1);*/
    }

    // 课程笔记公开数量
    public void courseNoteOpenNum() {
        //企业端
        List<HashMap<String, Object>> list = baseService.courseNoteOpenNum();
        checkListNull(list);
        //平台端
        /*List<String> idList = baseService.getAllNotLocalCourseId();
        List<HashMap<String, Object>> list1 = iPlatFormBaseService.courseNoteOpenNum(idList);
        checkListNull(list1);*/
    }


    private void checkListNull(List<HashMap<String, Object>> mapInfo) {
        if (null != mapInfo) {
            for (HashMap<String, Object> map : mapInfo) {
                Iterator iterator = map.entrySet().iterator();
                while (iterator.hasNext()) {
                    Map.Entry entry = (Map.Entry) iterator.next();
                    String key = (String) entry.getKey();
                    if (!StringUtils.isNotBlank(key)) {
                        iterator.remove();
                    }
                }
            }
            courseService.saveCourse(mapInfo);
        }
    }

}



