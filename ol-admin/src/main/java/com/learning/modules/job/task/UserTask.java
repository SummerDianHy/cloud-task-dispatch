package com.learning.modules.job.task;

import com.learning.modules.sys.service.base.IBaseService;
import com.learning.modules.sys.service.base.IPlatFormBaseService;
import com.learning.modules.sys.service.statistics.IStatisticsUserDailyService;
import com.learning.modules.sys.service.statistics.IStatisticsUserService;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * create by: JunAo.Yuan
 * description:用户数据统计 - 定时任务
 * create time: 16:51 2018/10/20
 *
 */
@Component("userTask")
public class UserTask {

    private static final Logger logger = LoggerFactory.getLogger(UserTask.class);

    // 统计数据 查询企业端
    @Autowired
    private IBaseService baseService;
    // 统计数据 查询平台端
    @Autowired
    private IPlatFormBaseService iPlatFormBaseService;


    // 课程统计
    @Autowired
    private IStatisticsUserService userService;

    // 学习时长
    public void learningTime() {
        //企业端
        List<HashMap<String, Object>> list = baseService.learningTime();
        checkListNull(list);
    }

    // 课程数
    public void courseNums() {
        //企业端
        List<HashMap<String, Object>> list = baseService.courseNums();
        checkListNull(list);
        //平台端
//        List<String> idList = baseService.getAllNotLocalCourseId();
//        List<HashMap<String, Object>> list1 = iPlatFormBaseService.favoriteNumDay(idList);
//        checkListNull(list1);
    }

    // 课程学习完成数
    public void courseFinishedUser() {
        //企业端
        List<HashMap<String, Object>> list = baseService.courseFinishedUser();
        checkListNull(list);

    }
    // 笔记数量
    public void noteNums() {
        //企业端
        List<HashMap<String, Object>> list = baseService.noteNums();
        checkListNull(list);
        //平台端
//        List<String> idList = baseService.getAllNotLocalCourseId();
//        List<HashMap<String, Object>> list1 = iPlatFormBaseService.studentNumDay(idList);
//        checkListNull(list1);
    }

    // 课程公开笔记数统计
    public void noteOpenNums() {
        //企业端
        List<HashMap<String, Object>> list = baseService.noteOpenNums();
        checkListNull(list);
        //平台端
//        List<String> idList = baseService.getAllNotLocalCourseId();
//        List<HashMap<String, Object>> list1 = iPlatFormBaseService.finishStudyNumDay(idList);
//        checkListNull(list1);
    }

    // 课程评论数统计
    public void topicNums() {
        //企业端
        List<HashMap<String, Object>> list = baseService.topicNums();
        checkListNull(list);
        //平台端
//        List<String> idList = baseService.getAllNotLocalCourseId();
//        List<HashMap<String, Object>> list1 = iPlatFormBaseService.viewNumDay(idList);
//        checkListNull(list1);
    }

    // 课程评论回复数统计
    public void topicReplyNums() {
        //企业端
        List<HashMap<String, Object>> list = baseService.topicReplyNums();
        checkListNull(list);
        //平台端
//        List<String> idList = baseService.getAllNotLocalCourseId();
//        List<HashMap<String, Object>> list1 = iPlatFormBaseService.noteNumDay(idList);
//        checkListNull(list1);
    }

    // 课程收藏数统计
    public void courseCollection() {
        //企业端
        List<HashMap<String, Object>> list = baseService.courseCollection();
        checkListNull(list);
    }

    // 课程评价次数
    public void courseJudge() {
        //企业端
        List<HashMap<String, Object>> list = baseService.courseJudge();
        checkListNull(list);
    }

    // 私信数统计
    public void privateMessage() {
        //企业端
        List<HashMap<String, Object>> list = baseService.privateMessage();
        checkListNull(list);
        //平台端
//        List<String> idList = baseService.getAllNotLocalCourseId();
//        List<HashMap<String, Object>> list1 = iPlatFormBaseService.courseJudgeNumDay(idList);
//        checkListNull(list1);
    }

    private void checkListNull(List<HashMap<String, Object>> mapInfo) {
        if (null != mapInfo) {
            for (HashMap<String, Object> map : mapInfo) {
                Iterator iterator = map.entrySet().iterator();
                while (iterator.hasNext()) {
                    Map.Entry entry = (Map.Entry) iterator.next();
                    String key = (String) entry.getKey();
                    if (!StringUtils.isNotBlank(key)) {
                        iterator.remove();
                    }
                }
            }
            userService.saveUser(mapInfo);
        }
    }

}



