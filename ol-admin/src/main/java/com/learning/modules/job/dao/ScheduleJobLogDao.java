package com.learning.modules.job.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.learning.modules.job.entity.ScheduleJobLogEntity;


/**
 * <p>
 *
 *    定时任务日志
 *
 * @description: </p>
 * @author: Kevin
 * @since: Created in 下午8:08 2018/3/31
 */
public interface ScheduleJobLogDao extends BaseMapper<ScheduleJobLogEntity> {
	
}
