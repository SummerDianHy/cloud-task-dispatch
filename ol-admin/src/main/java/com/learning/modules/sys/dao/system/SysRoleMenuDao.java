package com.learning.modules.sys.dao.system;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.learning.modules.sys.entity.system.SysRoleMenuEntity;

import java.util.List;


/**
 * <p>
 *
 *    角色与菜单对应的关系
 *
 * @description: </p>
 * @author: Kevin
 * @since: Created in 下午8:08 2018/3/31
 */
public interface SysRoleMenuDao extends BaseMapper<SysRoleMenuEntity> {
	
	/**
	 * 根据角色ID，获取菜单ID列表
	 */
	List<Long> queryMenuIdList(Long roleId);

	/**
	 * 根据角色ID数组，批量删除
	 */
	int deleteBatch(Long[] roleIds);
}
