package com.learning.modules.sys.dao.system;


import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.learning.modules.sys.entity.system.SysLogEntity;


/**
 * <p>
 *
 *    系统日志
 *
 * @description: </p>
 * @author: Kevin
 * @since: Created in 下午8:08 2018/3/31
 */
public interface SysLogDao extends BaseMapper<SysLogEntity> {
	
}
