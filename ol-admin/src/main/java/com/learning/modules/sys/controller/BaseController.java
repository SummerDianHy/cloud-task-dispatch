package com.learning.modules.sys.controller;

import com.baomidou.mybatisplus.plugins.Page;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.ModelAttribute;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


/**
 * <p>
 *  基础 控制器
 * </p>
 *
 * @author Kevin
 * @since 2018-3-17
 */
public class BaseController {

    protected final Logger logger = LoggerFactory.getLogger(this.getClass());

    protected HttpServletRequest request;

    protected HttpServletResponse response;

    @ModelAttribute
    public void setRequest(HttpServletRequest request) {
        this.request = request;
    }

    @ModelAttribute
    public void setResponse(HttpServletResponse response) {
        this.response = response;
    }

    protected <T> Page<T> getPage() {
        return getPage(10);
    }

    protected Object formatPage(Page<?> page) {
        return page;
    }

    protected <T> Page<T> getPage(int defaultSie) {
        String _size = request.getParameter("_size");
        String _index = request.getParameter("_index");
        int size = StringUtils.isNotBlank(_size) ? Integer.parseInt(_size) : defaultSie;
        int current = StringUtils.isNotBlank(_index) ? Integer.parseInt(_index) : 1;
        return new Page<>(current, size);
    }

}

