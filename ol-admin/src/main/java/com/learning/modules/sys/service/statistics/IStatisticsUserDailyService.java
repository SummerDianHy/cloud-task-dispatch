package com.learning.modules.sys.service.statistics;

import java.util.HashMap;
import java.util.List;

/**
 * <p>
 *     用户统计 每日 接口层
 *
 * @description: </p>
 * @author: Kevin
 * @since: Created in 下午8:54 2018/5/14
 */
public interface IStatisticsUserDailyService {

    /**
     * 课程相关统计 保存方法
     * @param courseMap
     */
    void saveUserDaily(List<HashMap<String, Object>> courseMap);

}
