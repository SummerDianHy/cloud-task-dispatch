package com.learning.modules.sys.service.system;

import com.baomidou.mybatisplus.service.IService;
import com.learning.common.utils.PageUtils;
import com.learning.modules.sys.entity.system.SysLogEntity;

import java.util.Map;

/**
 * <p>
 *
 *    系统日志
 *
 * @description: </p>
 * @author: Kevin
 * @since: Created in 下午8:08 2018/3/31
 */
public interface SysLogService extends IService<SysLogEntity> {

    PageUtils queryPage(Map<String, Object> params);

}
