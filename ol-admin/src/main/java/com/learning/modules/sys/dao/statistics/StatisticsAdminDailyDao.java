package com.learning.modules.sys.dao.statistics;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.learning.modules.sys.entity.statistics.StatisticsAdminDaily;

/**
 * <p>
 *     课程统计 dao
 *
 * @description: </p>
 * @author: Kevin
 * @since: Created in 下午2:03 2018/5/15
 */
public interface StatisticsAdminDailyDao extends BaseMapper<StatisticsAdminDaily> {
}
