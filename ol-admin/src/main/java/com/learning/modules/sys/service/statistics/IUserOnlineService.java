package com.learning.modules.sys.service.statistics;

import com.learning.modules.sys.entity.userOnline.UserOnline;

import java.util.HashMap;
import java.util.List;

/**
 * <p>
 *     用户在线 接口层
 *
 * @description: </p>
 * @author: Kevin
 * @since: Created in 下午8:54 2018/5/14
 */
public interface IUserOnlineService {

    /**
     * 更改用户在线状态
     * @param
     */
    void updateUserOnline(List<UserOnline> userOnlineList);

    /**
     * 更改商户到期状态（企业端）
     * @param
     */
    void updateMerchantStatus(List<HashMap<String, Object>> list);

    /**
     * 更改商户到期状态（平台端）
     * @param
     */
    void updateMerchantStatusTwo(List<HashMap<String, Object>> list);

}
