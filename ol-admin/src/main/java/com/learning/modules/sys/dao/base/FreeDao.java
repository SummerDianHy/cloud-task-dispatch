package com.learning.modules.sys.dao.base;

import com.learning.datasources.DataSourceNames;
import com.learning.datasources.annotation.DataSource;
import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

/**
 * 费用 mapper
 */
public interface FreeDao {

    List<HashMap<String, Object>> getMerchantInfo();

    @DataSource(name = DataSourceNames.THIRD)
    List<HashMap<String, Object>> getCDNFreeRecord();

    List<HashMap<String, Object>> getSMSFreeRecord();

    List<HashMap<String, Date>> getCDNFreeRecordGroupByDay();

    void updateMerchantFree(@Param("merchantKey") String merchantKey,@Param("free") BigDecimal free);
}
