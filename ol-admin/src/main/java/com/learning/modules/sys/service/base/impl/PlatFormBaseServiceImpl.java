package com.learning.modules.sys.service.base.impl;

import com.learning.datasources.DataSourceNames;
import com.learning.datasources.annotation.DataSource;
import com.learning.modules.sys.dao.base.BaseDao;
import com.learning.modules.sys.dao.base.PlatFormBaseDao;
import com.learning.modules.sys.entity.statistics.StatisticsUser;
import com.learning.modules.sys.entity.userOnline.UserOnline;
import com.learning.modules.sys.service.base.IBaseService;
import com.learning.modules.sys.service.base.IPlatFormBaseService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * <p>
 * 查询数据公共 层
 *
 * @description: </p>
 * @author: Kevin
 * @since: Created in 下午2:45 2018/5/15
 */
@Service
public class PlatFormBaseServiceImpl implements IPlatFormBaseService {

    @Autowired
    private PlatFormBaseDao baseDao;

    /**
     * 强调:
     *  1. 统计相关 读取数据及写入 都需要加上数据源注解, 如此方法
     * @return
     */

    /**************************************课程统计 start**************************************/

    /**
     * 统计课程收入
     *
     * @return
     */
    @DataSource(name = DataSourceNames.THIRD)
    @Override
    public List<HashMap<String, Object>> countCourseIncome() {
        return baseDao.countCourseIncome();
    }

    /**
     * 收藏人数
     *
     * @return
     */
    @DataSource(name = DataSourceNames.THIRD)
    @Override
    public List<HashMap<String, Object>> favoriteNum(List<String> courseId) {
        return baseDao.countCourseFavoriteNum(courseId);
    }

    /**
     * 学员数
     *
     * @return
     */
    @DataSource(name = DataSourceNames.THIRD)
    @Override
    public List<HashMap<String, Object>> studentNum(List<String> courseId) {
        return baseDao.countStudentNum(courseId);
    }

    /**
     * 学习人数
     *
     * @return
     */
    @DataSource(name = DataSourceNames.THIRD)
    @Override
    public List<HashMap<String, Object>> studyNum() {
        return baseDao.countStudyNum();
    }

    /**
     * 完成学员数
     *
     * @return
     */
    @DataSource(name = DataSourceNames.THIRD)
    @Override
    public List<HashMap<String, Object>> finishStudyNum(List<String> courseId) {
        return baseDao.countFinishStudyNum(courseId);
    }

    /**
     * 浏览次数
     *
     * @return
     */
    @DataSource(name = DataSourceNames.THIRD)
    @Override
    public List<HashMap<String, Object>> viewNum(List<String> courseId) {
        return baseDao.countCourseViewNum(courseId);
    }


    /**
     * 笔记数量
     *
     * @return
     */
    @DataSource(name = DataSourceNames.THIRD)
    @Override
    public List<HashMap<String, Object>> noteNum(List<String> courseId) {
        return baseDao.countCourseNoteNum(courseId);
    }

    /**
     * 课程评论分数
     *
     * @return
     */
    @DataSource(name = DataSourceNames.THIRD)
    @Override
    public List<HashMap<String, Object>> courseRating() {
        return baseDao.courseRating();
    }

    /**
     * 课程好评次数
     *
     * @return
     */
    @DataSource(name = DataSourceNames.THIRD)
    @Override
    public List<HashMap<String, Object>> judgeGoodNum() {
        return baseDao.judgeGoodNum();
    }

    /**
     * 课程评论次数
     *
     * @return
     */
    @DataSource(name = DataSourceNames.THIRD)
    @Override
    public List<HashMap<String, Object>> courseJudgeNum(List<String> courseId) {
        return baseDao.courseJudgeNum(courseId);
    }

    /**
     * 课程话题数量
     *
     * @return
     */
    @DataSource(name = DataSourceNames.THIRD)
    @Override
    public List<HashMap<String, Object>> courseTopicNum(List<String> courseId) {
        return baseDao.courseTopicNum(courseId);
    }

    /**
     * 话题回复数量
     *
     * @return
     */
    @DataSource(name = DataSourceNames.THIRD)
    @Override
    public List<HashMap<String, Object>> courseTopicReplyNum(List<String> courseId) {
        return baseDao.courseTopicReplyNum(courseId);
    }

    /**
     * 课程笔记公开数量
     *
     * @return
     */
    @DataSource(name = DataSourceNames.THIRD)
    @Override
    public List<HashMap<String, Object>> courseNoteOpenNum(List<String> courseId) {
        return baseDao.courseNoteOpenNum(courseId);
    }

    /**************************************课程统计 end**************************************/

    /**************************************课程统计 每日 start **************************************/
    /**
     * 统计课程收入
     *
     * @return
     */
    @DataSource(name = DataSourceNames.THIRD)
    @Override
    public List<HashMap<String, Object>> countCourseIncomeDay() {
        return baseDao.countCourseIncomeDay();
    }

    /**
     * 收藏人数
     *
     * @return
     */
    @DataSource(name = DataSourceNames.THIRD)
    @Override
    public List<HashMap<String, Object>> favoriteNumDay(List<String> courseId) {
        return baseDao.countCourseFavoriteNumDay(courseId);
    }

    /**
     * 学员数
     *
     * @return
     */
    @DataSource(name = DataSourceNames.THIRD)
    @Override
    public List<HashMap<String, Object>> studentNumDay(List<String> courseId) {
        return baseDao.countStudentNumDay(courseId);
    }

    /**
     * 学习人数
     *
     * @return
     */
    @DataSource(name = DataSourceNames.THIRD)
    @Override
    public List<HashMap<String, Object>> studyNumDay() {
        return baseDao.countStudyNumDay();
    }

    /**
     * 完成学员数
     *
     * @return
     */
    @DataSource(name = DataSourceNames.THIRD)
    @Override
    public List<HashMap<String, Object>> finishStudyNumDay(List<String> courseId) {
        return baseDao.countFinishStudyNumDay(courseId);
    }

    /**
     * 浏览次数
     *
     * @return
     */
    @DataSource(name = DataSourceNames.THIRD)
    @Override
    public List<HashMap<String, Object>> viewNumDay(List<String> courseId) {
        return baseDao.countCourseViewNumDay(courseId);
    }


    /**
     * 笔记数量
     *
     * @return
     */
    @DataSource(name = DataSourceNames.THIRD)
    @Override
    public List<HashMap<String, Object>> noteNumDay(List<String> courseId) {
        return baseDao.countCourseNoteNumDay(courseId);
    }

    /**
     * 课程评论分数
     *
     * @return
     */
    @DataSource(name = DataSourceNames.THIRD)
    @Override
    public List<HashMap<String, Object>> courseRatingDay() {
        return baseDao.courseRatingDay();
    }

    /**
     * 课程好评次数
     *
     * @return
     */
    @DataSource(name = DataSourceNames.THIRD)
    @Override
    public List<HashMap<String, Object>> judgeGoodNumDay() {
        return baseDao.judgeGoodNumDay();
    }

    /**
     * 课程评论次数
     *
     * @return
     */
    @DataSource(name = DataSourceNames.THIRD)
    @Override
    public List<HashMap<String, Object>> courseJudgeNumDay(List<String> courseId) {
        return baseDao.courseJudgeNumDay(courseId);
    }

    /**
     * 课程话题数量
     *
     * @return
     */
    @DataSource(name = DataSourceNames.THIRD)
    @Override
    public List<HashMap<String, Object>> courseTopicNumDay(List<String> courseId) {
        return baseDao.courseTopicNumDay(courseId);
    }

    /**
     * 话题回复数量
     *
     * @return
     */
    @DataSource(name = DataSourceNames.THIRD)
    @Override
    public List<HashMap<String, Object>> courseTopicReplyNumDay(List<String> courseId) {
        return baseDao.courseTopicReplyNumDay(courseId);
    }

    /**
     * 课程笔记公开数量
     *
     * @return
     */
    @DataSource(name = DataSourceNames.THIRD)
    @Override
    public List<HashMap<String, Object>> courseNoteOpenNumDay(List<String> courseId) {
        return baseDao.courseNoteOpenNumDay(courseId);
    }

    /**************************************课程统计 end**************************************/

    /**************************************企业统计 start**************************************/
    @DataSource(name = DataSourceNames.THIRD)
    @Override
    public List<HashMap<String, Object>> accountNums() {
        return baseDao.countAccountNum();
    }

    @DataSource(name = DataSourceNames.THIRD)
    @Override
    public List<HashMap<String, Object>> students() {
        return baseDao.countStudents();
    }

    @DataSource(name = DataSourceNames.THIRD)
    @Override
    public List<HashMap<String, Object>> loginNums() {
        return baseDao.countLoginNums();
    }

    @DataSource(name = DataSourceNames.THIRD)
    @Override
    public List<HashMap<String, Object>> accountNouser() {
        return baseDao.countAccountNouser();
    }

    @DataSource(name = DataSourceNames.THIRD)
    @Override
    public List<HashMap<String, Object>> studyNums() {
        return baseDao.countStudyNums();
    }

    @DataSource(name = DataSourceNames.THIRD)
    @Override
    public List<HashMap<String, Object>> studyStuNum() {
        return baseDao.countStudyStuNum();
    }

    @DataSource(name = DataSourceNames.THIRD)
    @Override
    public List<HashMap<String, Object>> courseLearning() {
        return baseDao.countCourseLearning();
    }

    @DataSource(name = DataSourceNames.THIRD)
    @Override
    public List<HashMap<String, Object>> courseFinished() {
        return baseDao.countCourseFinished();
    }

    @DataSource(name = DataSourceNames.THIRD)
    @Override
    public List<HashMap<String, Object>> stuCourseFinished() {
        return baseDao.countStuCourseFinished();
    }

    @DataSource(name = DataSourceNames.THIRD)
    @Override
    public List<HashMap<String, Object>> stuExamWait() {
        return baseDao.countStuExamWait();
    }

    @DataSource(name = DataSourceNames.THIRD)
    @Override
    public List<HashMap<String, Object>> stuExamFinished() {
        return baseDao.countStuExamFinished();
    }

    @DataSource(name = DataSourceNames.THIRD)
    @Override
    public List<HashMap<String, Object>> stuExamPass() {
        return baseDao.countStuExamPass();
    }

    @DataSource(name = DataSourceNames.THIRD)
    @Override
    public List<HashMap<String, Object>> stuExamNopass() {
        return baseDao.countStuExamNopass();
    }

    @DataSource(name = DataSourceNames.THIRD)
    @Override
    public List<HashMap<String, Object>> stuNoteNums() {
        return baseDao.countStuNoteNums();
    }

    @DataSource(name = DataSourceNames.THIRD)
    @Override
    public List<HashMap<String, Object>> stuTopicNums() {
        return baseDao.countStuTopicNums();
    }

    @DataSource(name = DataSourceNames.THIRD)
    @Override
    public List<HashMap<String, Object>> stuTopicReplyNums() {
        return baseDao.countStuTopicReplyNums();
    }

    @DataSource(name = DataSourceNames.THIRD)
    @Override
    public List<HashMap<String, Object>> stuVideoPlays() {
        return baseDao.countStuVideoPlays();
    }

    @DataSource(name = DataSourceNames.THIRD)
    @Override
    public List<HashMap<String, Object>> stuVideoFinished() {
        return baseDao.countStuVideoFinished();
    }

    @DataSource(name = DataSourceNames.THIRD)
    @Override
    public List<HashMap<String, Object>> stuCourseViews() {
        return baseDao.countStuCourseViews();
    }

    /**************************************企业统计 end**************************************/

    /**************************************企业日计 start**************************************/

    @DataSource(name = DataSourceNames.THIRD)
    @Override
    public List<HashMap<String, Object>> accountNumsDaily() {
        return baseDao.countAccountNumDaily();
    }

    @DataSource(name = DataSourceNames.THIRD)
    @Override
    public List<HashMap<String, Object>> loginNumsDaily() {
        return baseDao.countLoginNumsDaily();
    }

    @DataSource(name = DataSourceNames.THIRD)
    @Override
    public List<HashMap<String, Object>> studyNumsDaily() {
        return baseDao.countStudyNumsDaily();
    }

    @DataSource(name = DataSourceNames.THIRD)
    @Override
    public List<HashMap<String, Object>> studyStuNumDaily() {
        return baseDao.countStudyStuNumDaily();
    }

    @DataSource(name = DataSourceNames.THIRD)
    @Override
    public List<HashMap<String, Object>> courseLearningDaily() {
        return baseDao.countCourseLearningDaily();
    }

    @DataSource(name = DataSourceNames.THIRD)
    @Override
    public List<HashMap<String, Object>> courseFinishedDaily() {
        return baseDao.countCourseFinishedDaily();
    }

    @DataSource(name = DataSourceNames.THIRD)
    @Override
    public List<HashMap<String, Object>> stuCourseFinishedDaily() {
        return baseDao.countStuCourseFinishedDaily();
    }

    @DataSource(name = DataSourceNames.THIRD)
    @Override
    public List<HashMap<String, Object>> stuExamWaitDaily() {
        return baseDao.countStuExamWaitDaily();
    }

    @DataSource(name = DataSourceNames.THIRD)
    @Override
    public List<HashMap<String, Object>> stuExamFinishedDaily() {
        return baseDao.countStuExamFinishedDaily();
    }

    @DataSource(name = DataSourceNames.THIRD)
    @Override
    public List<HashMap<String, Object>> stuExamPassDaily() {
        return baseDao.countStuExamPassDaily();
    }

    @DataSource(name = DataSourceNames.THIRD)
    @Override
    public List<HashMap<String, Object>> stuExamNopassDaily() {
        return baseDao.countStuExamNopassDaily();
    }

    @DataSource(name = DataSourceNames.THIRD)
    @Override
    public List<HashMap<String, Object>> stuNoteNumsDaily() {
        return baseDao.countStuNoteNumsDaily();
    }

    @DataSource(name = DataSourceNames.THIRD)
    @Override
    public List<HashMap<String, Object>> stuTopicNumsDaily() {
        return baseDao.countStuTopicNumsDaily();
    }

    @DataSource(name = DataSourceNames.THIRD)
    @Override
    public List<HashMap<String, Object>> stuTopicReplyNumsDaily() {
        return baseDao.countStuTopicReplyNumsDaily();
    }

    @DataSource(name = DataSourceNames.THIRD)
    @Override
    public List<HashMap<String, Object>> stuVideoPlaysDaily() {
        return baseDao.countStuVideoPlaysDaily();
    }

    @DataSource(name = DataSourceNames.THIRD)
    @Override
    public List<HashMap<String, Object>> stuVideoFinishedDaily() {
        return baseDao.countStuVideoFinishedDaily();
    }

    @DataSource(name = DataSourceNames.THIRD)
    @Override
    public List<HashMap<String, Object>> stuCourseViewsDaily() {
        return baseDao.countStuCourseViewsDaily();
    }


    /**************************************企业日计 end**************************************/

    /**************************************平台统计 start**************************************/
    @DataSource(name = DataSourceNames.THIRD)
    @Override
    public HashMap<String, Object> accountNumsAdmin() {
        return baseDao.accountNumsAdmin();
    }

    @DataSource(name = DataSourceNames.THIRD)
    @Override
    public HashMap<String, Object> loginNumsAdmin() {
        return baseDao.loginNumsAdmin();
    }

    @DataSource(name = DataSourceNames.THIRD)
    @Override
    public HashMap<String, Object> studentNumAdmin() {
        return baseDao.studentNumAdmin();
    }

    @DataSource(name = DataSourceNames.THIRD)
    @Override
    public HashMap<String, Object> orgStudentNumsAdmin() {
        return baseDao.orgStudentNumsAdmin();
    }

    @DataSource(name = DataSourceNames.THIRD)
    @Override
    public HashMap<String, Object> orgNumAdmin() {
        return baseDao.orgNumAdmin();
    }

    @DataSource(name = DataSourceNames.THIRD)
    @Override
    public HashMap<String, Object> orgRegisterNumAdmin() {
        return baseDao.orgRegisterNumAdmin();
    }

    @DataSource(name = DataSourceNames.THIRD)
    @Override
    public HashMap<String, Object> orgAccountNumsAdmin() {
        return baseDao.orgAccountNumsAdmin();
    }

    @DataSource(name = DataSourceNames.THIRD)
    @Override
    public HashMap<String, Object> orgAccountPaymentAdmin() {
        return baseDao.orgAccountPaymentAdmin();
    }

    @DataSource(name = DataSourceNames.THIRD)
    @Override
    public HashMap<String, Object> orgAccountUsedAdmin() {
        return baseDao.orgAccountUsedAdmin();
    }

    @DataSource(name = DataSourceNames.THIRD)
    @Override
    public HashMap<String, Object> courseOrderAdmin() {
        return baseDao.courseOrderAdmin();
    }

    @DataSource(name = DataSourceNames.THIRD)
    @Override
    public HashMap<String, Object> courseOrderPaymentAdmin() {
        return baseDao.courseOrderPaymentAdmin();
    }

    @DataSource(name = DataSourceNames.THIRD)
    @Override
    public HashMap<String, Object> orgCourseOrderAdmin() {
        return baseDao.orgCourseOrderAdmin();
    }

    @DataSource(name = DataSourceNames.THIRD)
    @Override
    public HashMap<String, Object> orgCourseOrderPaymentAdmin() {
        return baseDao.orgCourseOrderPaymentAdmin();
    }

//    @DataSource(name = DataSourceNames.THIRD)
//    @Override
//    public HashMap<String, Object> areaNumAdmin() {
//        return baseDao.areaNumAdmin();
//    }

    @DataSource(name = DataSourceNames.THIRD)
    @Override
    public HashMap<String, Object> areaAdminNumAdmin() {
        return baseDao.areaAdminNumAdmin();
    }

    @DataSource(name = DataSourceNames.THIRD)
    @Override
    public HashMap<String, Object> areaAdminLoginAdmin() {
        return baseDao.areaAdminLoginAdmin();
    }

    @DataSource(name = DataSourceNames.THIRD)
    @Override
    public HashMap<String, Object> privateMessageNumsAdmin() {
        return baseDao.privateMessageNumsAdmin();
    }

    @DataSource(name = DataSourceNames.THIRD)
    @Override
    public HashMap<String, Object> privateMessageReadAdmin() {
        return baseDao.privateMessageReadAdmin();
    }

    @DataSource(name = DataSourceNames.THIRD)
    @Override
    public HashMap<String, Object> announcementNumsAdmin() {
        return baseDao.announcementNumsAdmin();
    }

    @DataSource(name = DataSourceNames.THIRD)
    @Override
    public HashMap<String, Object> announcementReadAdmin() {
        return baseDao.announcementReadAdmin();
    }

    @DataSource(name = DataSourceNames.THIRD)
    @Override
    public HashMap<String, Object> noticeNumsAdmin() {
        return baseDao.noticeNumsAdmin();
    }

    @DataSource(name = DataSourceNames.THIRD)
    @Override
    public HashMap<String, Object> noticeReadAdmin() {
        return baseDao.noticeReadAdmin();
    }

    @DataSource(name = DataSourceNames.THIRD)
    @Override
    public List<Integer> selectOrderStatusForOutTime() {
        return baseDao.selectOrderStatusForOutTime();
    }

    @DataSource(name = DataSourceNames.THIRD)
    @Override
    public void updateUserOrderByOutTime(Map<String, Object> map) {
        baseDao.updateUserOrderByOutTime(map);
    }

    ;
    /**************************************平台统计 end**************************************/

    /**************************************用户统计 每日 start**************************************/
    @DataSource(name = DataSourceNames.THIRD)
    @Override
    public List<HashMap<String, Object>> courseNumsDaily() {
        return baseDao.courseNumsDaily();
    }

    @DataSource(name = DataSourceNames.THIRD)
    @Override
    public List<HashMap<String, Object>> courseFinishedUserDaily() {
        return baseDao.courseFinishedUserDaily();
    }

    @DataSource(name = DataSourceNames.THIRD)
    @Override
    public List<HashMap<String, Object>> noteNumsDaily() {
        return baseDao.noteNumsDaily();
    }

    @DataSource(name = DataSourceNames.THIRD)
    @Override
    public List<HashMap<String, Object>> noteOpenNumsDaily() {
        return baseDao.noteOpenNumsDaily();
    }

    @DataSource(name = DataSourceNames.THIRD)
    @Override
    public List<HashMap<String, Object>> topicNumsDaily() {
        return baseDao.topicNumsDaily();
    }

    @DataSource(name = DataSourceNames.THIRD)
    @Override
    public List<HashMap<String, Object>> topicReplyNumsDaily() {
        return baseDao.topicReplyNumsDaily();
    }

    @DataSource(name = DataSourceNames.THIRD)
    @Override
    public List<HashMap<String, Object>> learningTimeDaily() {
        return baseDao.learningTimeDaily();
    }

    @DataSource(name = DataSourceNames.THIRD)
    @Override
    public List<HashMap<String, Object>> courseCollectionDaily() {
        return baseDao.courseCollectionDaily();
    }

    @DataSource(name = DataSourceNames.THIRD)
    @Override
    public List<HashMap<String, Object>> courseJudgeDaily() {
        return baseDao.courseJudgeDaily();
    }

    @DataSource(name = DataSourceNames.THIRD)
    @Override
    public List<HashMap<String, Object>> privateMessageDaily() {
        return baseDao.privateMessageDaily();
    }

    /**************************************用户统计 end**************************************/

    /**************************************用户数据统计 start**************************************/
    @DataSource(name = DataSourceNames.THIRD)
    @Override
    public List<HashMap<String, Object>> courseNums() {
        return baseDao.courseNums();
    }

    @DataSource(name = DataSourceNames.THIRD)
    @Override
    public List<HashMap<String, Object>> courseFinishedUser() {
        return baseDao.courseFinishedUser();
    }

    @DataSource(name = DataSourceNames.THIRD)
    @Override
    public List<HashMap<String, Object>> noteNums() {
        return baseDao.noteNums();
    }

    @DataSource(name = DataSourceNames.THIRD)
    @Override
    public List<HashMap<String, Object>> noteOpenNums() {
        return baseDao.noteOpenNums();
    }

    @DataSource(name = DataSourceNames.THIRD)
    @Override
    public List<HashMap<String, Object>> topicNums() {
        return baseDao.topicNums();
    }

    @DataSource(name = DataSourceNames.THIRD)
    @Override
    public List<HashMap<String, Object>> topicReplyNums() {
        return baseDao.topicReplyNums();
    }

    @DataSource(name = DataSourceNames.THIRD)
    @Override
    public List<HashMap<String, Object>> learningTime() {
        return baseDao.learningTime();
    }

    @DataSource(name = DataSourceNames.THIRD)
    @Override
    public List<HashMap<String, Object>> courseCollection() {
        return baseDao.courseCollection();
    }

    @DataSource(name = DataSourceNames.THIRD)
    @Override
    public List<HashMap<String, Object>> courseJudge() {
        return baseDao.courseJudge();
    }

    @DataSource(name = DataSourceNames.THIRD)
    @Override
    public List<HashMap<String, Object>> privateMessage() {
        return baseDao.privateMessage();
    }


    /**************************************用户统计 end**************************************/

    /**************************************用户数据统计 start**************************************/
    @DataSource(name = DataSourceNames.THIRD)
    @Override
    public List<HashMap<String, Object>> signNumsUser() {
        return baseDao.signNumsUser();
    }

    @DataSource(name = DataSourceNames.THIRD)
    @Override
    public List<HashMap<String, Object>> courseLearningUser() {
        return baseDao.courseLearningUser();
    }

    @DataSource(name = DataSourceNames.THIRD)
    @Override
    public List<HashMap<String, Object>> examPercentUser() {
        return baseDao.examPercentUser();
    }

    @DataSource(name = DataSourceNames.THIRD)
    @Override
    public List<HashMap<String, Object>> examWaitUser() {
        return baseDao.examWaitUser();
    }

    @DataSource(name = DataSourceNames.THIRD)
    @Override
    public StatisticsUser selectOneByUserId(Integer userId) {
        return baseDao.selectOneByUserId(userId);
    }

    @DataSource(name = DataSourceNames.THIRD)
    @Override
    public void insertUser(StatisticsUser user) {
        baseDao.insertUser(user);
    }

    @DataSource(name = DataSourceNames.THIRD)
    @Override
    public void updateUser(StatisticsUser user) {
        baseDao.updateUser(user);
    }

    @DataSource(name = DataSourceNames.THIRD)
    @Override
    public void updateUserOnline(UserOnline userOnline) {
        UserOnline online = new UserOnline();
        online.setOnlineTime(userOnline.getOnlineTime());
        online.setIsOnline(0);
        userOnline = baseDao.selectOnlineUser(userOnline);
        if (userOnline != null && userOnline.getId() != null) {
            online.setId(userOnline.getId());
            if (userOnline.getUserId() != null) {
                online.setUserId(userOnline.getUserId());
            }
            if (StringUtils.isNotBlank(userOnline.getSessionId())) {
                online.setSessionId(userOnline.getSessionId());
            }
            baseDao.updateUserOnline(online);
        }
    }

    @DataSource(name = DataSourceNames.THIRD)
    @Override
    public List<UserOnline> selectOnlineUserList() {
        return baseDao.selectOnlineUserList();
    }

    @DataSource(name = DataSourceNames.THIRD)
    @Override
    public List<HashMap<String, Object>> selectOnline() {
        return baseDao.selectOnline();
    }


    /**************************************用户数据统计 end**************************************/


    /**************************************商户数据统计 start**************************************/
    @DataSource(name = DataSourceNames.THIRD)
    @Override
    public List<HashMap<String, Object>> selectEnableMerchantList() {
        return baseDao.selectEnableMerchantList();
    }

    @DataSource(name = DataSourceNames.THIRD)
    @Override
    public void updateMerchantStatus(List<Long> ids) {
        baseDao.updateMerchantStatus(ids);
    }
}
