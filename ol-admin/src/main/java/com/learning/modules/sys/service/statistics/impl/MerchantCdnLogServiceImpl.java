package com.learning.modules.sys.service.statistics.impl;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.learning.common.utils.CDNConstant;
import com.learning.common.utils.GetUTCTimeUtil;
import com.learning.common.utils.HttpRequest;
import com.learning.common.utils.alibaba.SignatureUtils;
import com.learning.datasources.DataSourceNames;
import com.learning.datasources.annotation.DataSource;
import com.learning.modules.sys.dao.base.FreeDao;
import com.learning.modules.sys.dao.statistics.MerchantCdnLogFreeDao;
import com.learning.modules.sys.dao.statistics.StatisticsFreeDao;
import com.learning.modules.sys.entity.statistics.MerchantCDNLog;
import com.learning.modules.sys.entity.statistics.StatisticsFree;
import com.learning.modules.sys.service.base.IFreeService;
import com.learning.modules.sys.service.statistics.IMerchantCdnLogService;
import com.learning.modules.sys.service.statistics.IStatisticsFreeService;
import io.swagger.models.auth.In;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * <p>
 * CDN费用发生记录 实现层
 *
 * @description: </p>
 * @author: JunAo.Yuan
 * @since: Created in 下午8:55 2018/11/10
 */
@Service
public class MerchantCdnLogServiceImpl extends ServiceImpl<MerchantCdnLogFreeDao, MerchantCDNLog> implements IMerchantCdnLogService {

    @Autowired
    IFreeService iFreeService;

    @Autowired
    MerchantCdnLogFreeDao merchantCdnLogFreeDao;

    @Autowired
    FreeDao freeDao;

    /**
     * 查询费用记录 cdn
     * @return
     */
    @DataSource(name = DataSourceNames.THIRD)
    @Override
    public void saveCDNDaily(String date) {
        /*获取cloud cdn加速详情*/
        String info = "";
        try {
            if (StringUtils.isNotEmpty(date)) {
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                info = getMerchantCdnInfo(CDNConstant.domain, sdf.parse(date));
            } else {
                info = getMerchantCdnInfo(CDNConstant.domain, null);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        JSONObject object = JSONObject.parseObject(info);
        if (object != null) {
            List<HashMap<String, Object>> merchantList = freeDao.getMerchantInfo();//获取所有商户
            for (int i = 0; i < merchantList.size(); i++) {
                HashMap<String, Object> merchant = merchantList.get(i);
                String appKey = merchant.get("appKey").toString();
                String merchantDomain = merchant.get("merchantDomain").toString();

                try {
                    /*返回参数解析*/
                    String RequestId = object.getString("RequestId");//请求ID
                    String DomainName = object.getString("DomainName");//加速域名信息(商户)
                    String StartTime = object.getString("StartTime");//查询指定日期

                    JSONObject TopReferList = (JSONObject)object.get("TopReferList");
                    JSONArray ReferList = TopReferList.getJSONArray("ReferList");
                    int visitNum = 0;//访问次数
                    double flow = 0;//访问流量byte
                    if (null != ReferList && ReferList.size() > 0) {
                        for (int j = 0; j < ReferList.size(); j++) {
                            JSONObject refer = ReferList.getJSONObject(j);
                            String VisitData = refer.getString("VisitData");//访问次数
                            String ReferDetail = refer.getString("ReferDetail");//完整的ReferUrl地址
                            String VisitProportion = refer.getString("VisitProportion");//访问占比
                            String Flow = refer.getString("Flow");//流量。单位：byte
                            Flow = Flow.substring(0,Flow.indexOf(".")+2);

                            String FlowProportion = refer.getString("FlowProportion");//流量占比
                            if (ReferDetail.equals(merchantDomain)){//匹配商户域名
                                visitNum += Integer.parseInt(VisitData);
                                flow += Double.parseDouble(Flow);
                            }
                        }
                    }
                    //不管有没有CDN加速，都加一条当天数据
                    MerchantCDNLog log = new MerchantCDNLog();
                    log.setDomainName(merchantDomain);
                    SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                    if (StringUtils.isNotEmpty(date)) {//查指定日期，阿里云返回UTC格式，需转换
                        TimeZone utcZone = TimeZone.getTimeZone("UTC");
                        df.setTimeZone(utcZone);
                        Date myDate = df.parse(StartTime);
                        log.setFindTime(myDate);
                    } else {
                        log.setFindTime(df.parse(StartTime));
                    }
                    log.setFlow(String.valueOf(flow));
                    log.setReferer(DomainName);
                    log.setVisitdata(String.valueOf(visitNum));
                    log.setMerchantKey(appKey);
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                    String findTime =sdf.format(log.getFindTime());
                    MerchantCDNLog entity = this.selectOne(new EntityWrapper<MerchantCDNLog>().eq("merchant_key", appKey).eq("find_time", findTime));
                    if (null == entity) {
                        this.insert(log);
                    }else {
                        this.update(log,new EntityWrapper<MerchantCDNLog>().eq("id",entity.getId()));
                    }
                } catch (Exception e) {
                    System.out.println("--------CDN Free Exception--------appKey == "+appKey+"---domain == "+merchantDomain);
                    e.printStackTrace();
                }
            }
        }
    }

    /**
    *   根据商户域名获取商户CDN加速详情
     *   merchantDomain:商户域名，date：时间
    */
    public static String getMerchantCdnInfo(String merchantDomain,Date date) {
        String info = "";
        String Service = CDNConstant.Service;
        String AccessKeySecret = CDNConstant.AccessKeySecret;
        String AccessKeyId = CDNConstant.AccessKeyId;
        String Format = CDNConstant.Format;
        String Version = CDNConstant.Version;
        String SignatureMethod = CDNConstant.SignatureMethod;
        String SignatureVersion = CDNConstant.SignatureVersion;
        String SignatureNonce=(new Date().getTime()+2345679)+"";
        String Timestamp= GetUTCTimeUtil.localToUTC(new Date());

        String Action = CDNConstant.Action;

        Map<String,String> param = new HashMap<>();
        param.put("AccessKeyId", AccessKeyId);
        param.put("Format", Format);
        param.put("Version", Version);
        param.put("SignatureMethod", SignatureMethod);
        param.put("SignatureVersion", SignatureVersion);
        param.put("SignatureNonce", SignatureNonce);
        param.put("Timestamp", Timestamp);
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'16:00:00'Z'");
        df.setTimeZone(TimeZone.getTimeZone("UTC"));
        if (null != date) {
            param.put("StartTime",df.format(date));//查询某天的数据，建议传YYYY-MM-DDT16:00:00Z 不写默认读取过去24小时数据。
        }else {
            param.put("StartTime",df.format(new Date()));//查当天的数据
        }
        param.put("DomainName", merchantDomain);
        param.put("Action", Action);
        try {
            String url = SignatureUtils.generate("GET",param,AccessKeySecret);//签名
            System.out.println("url=="+url);
            String paramStr = SignatureUtils.generateQueryString(param, true);
            paramStr = paramStr+"&Signature="+url;
            System.out.println(paramStr);
            info = HttpRequest.sendGet(Service,paramStr);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return info;
    }

    public static void main(String[] args) {
//        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//        String info = null;
//        try {
//            info = getMerchantCdnInfo("img.ol-learning.com",null);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        System.out.println("结果为："+

        // 日期格式转换
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        // 当前日期
        Calendar instance = Calendar.getInstance();
        // 调整到上周
//        instance.add(Calendar.WEDNESDAY, -1);
        // 调整到上周1
        instance.set(Calendar.DAY_OF_WEEK, 2);
        //循环打印
        for (int i = 1; i <= 7; i++) {
            System.out.println("星期" + i + ":" + format.format(instance.getTime()));
            instance.add(Calendar.DAY_OF_WEEK, 1);
        }

//        String number = "123.456E8";
//        String intNumber = number.substring(0,number.indexOf(".")+2);
//        double b = Double.parseDouble(intNumber);
//        double a = 2.3;
//        System.out.println(a+b);
    }
}
