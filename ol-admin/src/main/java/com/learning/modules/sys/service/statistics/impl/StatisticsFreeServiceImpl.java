package com.learning.modules.sys.service.statistics.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.learning.datasources.DataSourceNames;
import com.learning.datasources.annotation.DataSource;
import com.learning.modules.sys.dao.base.FreeDao;
import com.learning.modules.sys.dao.statistics.StatisticsFreeDao;
import com.learning.modules.sys.entity.statistics.StatisticsFree;
import com.learning.modules.sys.service.base.IFreeService;
import com.learning.modules.sys.service.statistics.IStatisticsFreeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * <p>
 * 费用结算实现层
 *
 * @description: </p>
 * @author: JunAo.Yuan
 * @since: Created in 下午8:55 2018/5/14
 */
@Service
public class StatisticsFreeServiceImpl extends ServiceImpl<StatisticsFreeDao, StatisticsFree> implements IStatisticsFreeService {

    @Autowired
    IFreeService iFreeService;

    @Autowired
    FreeDao freeDao;

    @Autowired
    StatisticsFreeDao statisticsFreeDao;

    /**
     * 费用结算 cdn
     * @return
     */
    @DataSource(name = DataSourceNames.THIRD)
    @Override
    public void saveFree(List<HashMap<String, Object>> freeMap) {
        try {
            for (HashMap<String, Object> map : freeMap) {
                StatisticsFree free = new StatisticsFree();
                if (map.get("merchantKey") != null) {
                    String merchantKey = map.get("merchantKey").toString();
                    free.setCreateTime(new Date());
                    free.setMerchantKey(merchantKey);
                    free.setFeeDesc("扣除商户本周CDN产生的费用");

//                    BigDecimal findTime = new BigDecimal(map.get("findTime").toString());//cdn统计时间
                    BigDecimal visitData = new BigDecimal(map.get("visitData").toString());//商户本周cdn访问次数
                    BigDecimal orgAccount = new BigDecimal(map.get("orgAccount").toString());//商户账户余额
                    BigDecimal cdnPrice = new BigDecimal(map.get("cdnPrice").toString());//cdn千次单价
                    BigDecimal cdnFree = visitData.multiply(cdnPrice).divide(new BigDecimal(1000));
                    BigDecimal leftFee = orgAccount.subtract(cdnFree);//商户扣除费用后余额

                    free.setBeforeFee(orgAccount);
                    free.setFee(cdnFree);
                    free.setLeftFee(leftFee);
                    free.setFeeType(4);//费用类型0：续费1：升级2：短信3：充值4：CDN 5:直播6：退款
                    Integer result = statisticsFreeDao.insert(free);
                    if (result > 0) {
                        freeDao.updateMerchantFree(merchantKey, cdnFree);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @DataSource(name = DataSourceNames.THIRD)
    private Integer insertFree(StatisticsFree free) {
        return statisticsFreeDao.insert(free);
    }

    /**
     * 费用结算 sms
     * @return
     */
    @DataSource(name = DataSourceNames.THIRD)
    @Override
    public void saveFreeTwo(List<HashMap<String, Object>> freeMap) {
        try {
            for (HashMap<String, Object> map : freeMap) {
                StatisticsFree free = new StatisticsFree();
                if (map.get("merchantKey") != null) {
                    String merchantKey = map.get("merchantKey").toString();
                    free.setCreateTime(new Date());
                    free.setMerchantKey(merchantKey);
                    free.setFeeDesc("扣除商户本周SMS产生的费用");

                    BigDecimal smsCount = new BigDecimal(map.get("smsCount").toString());//商户本周发送短信条数
                    BigDecimal orgAccount = new BigDecimal(map.get("orgAccount").toString());//商户账户余额
                    BigDecimal msgPrice = new BigDecimal(map.get("msgPrice").toString());//商户短信单价
                    BigDecimal smsFree = smsCount.multiply(msgPrice);
                    BigDecimal leftFee = orgAccount.subtract(smsFree);//商户扣除费用后余额

                    free.setBeforeFee(orgAccount);
                    free.setFee(smsFree);
                    free.setLeftFee(leftFee);
                    free.setFeeType(2);//费用类型0：续费1：升级2：短信3：充值4：CDN 5:直播6：退款
                    Integer result = statisticsFreeDao.insert(free);
                    if (result > 0) {
                        freeDao.updateMerchantFree(merchantKey, smsFree);
                    }
                }
            }
        } catch (Exception e) {

        }
    }

}
