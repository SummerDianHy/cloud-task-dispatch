package com.learning.modules.sys.dao.statistics;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.learning.modules.sys.entity.statistics.MerchantCDNLog;
import com.learning.modules.sys.entity.statistics.MerchantSmsLog;

/**
 * <p>
 *     CDN记录 dao
 *
 */
public interface MerchantSMSLogFreeDao extends BaseMapper<MerchantSmsLog> {

}
