package com.learning.modules.sys.dao.base;

import com.learning.modules.sys.entity.statistics.StatisticsUser;
import com.learning.modules.sys.entity.userOnline.UserOnline;
import org.apache.ibatis.annotations.Param;
import org.omg.CORBA.OBJ_ADAPTER;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * <p>
 * 查询数据公共 dao
 *
 * @description: </p>
 * @author: Kevin
 * @since: Created in 下午2:46 2018/5/15
 */
public interface BaseDao {
    List<String> getAllNotLocalCourseId();

    /**************************************课程统计 start*************************************/

    List<HashMap<String, Object>> countCourseIncome();

    List<HashMap<String, Object>> countCourseFavoriteNum();

    List<HashMap<String, Object>> countStudentNum();

    List<HashMap<String, Object>> countStudyNum();

    List<HashMap<String, Object>> countFinishStudyNum();

    List<HashMap<String, Object>> countCourseViewNum();

    List<HashMap<String, Object>> countCourseNoteNum();

    List<HashMap<String, Object>> courseRating();

    List<HashMap<String, Object>> judgeGoodNum();

    List<HashMap<String, Object>> courseJudgeNum();

    List<HashMap<String, Object>> courseTopicNum();

    List<HashMap<String, Object>> courseTopicReplyNum();

    List<HashMap<String, Object>> courseNoteOpenNum();

    /**************************************课程统计 end**************************************/

    /**************************************课程统计 每日 start*************************************/

    List<HashMap<String, Object>> countCourseIncomeDay(Date date);

    List<HashMap<String, Object>> countCourseFavoriteNumDay(Date date);

    List<HashMap<String, Object>> countStudentNumDay(Date date);

    List<HashMap<String, Object>> countStudyNumDay(Date date);

    List<HashMap<String, Object>> countFinishStudyNumDay(Date date);

    List<HashMap<String, Object>> countCourseViewNumDay(Date date);

    List<HashMap<String, Object>> countCourseNoteNumDay(Date date);

    List<HashMap<String, Object>> courseRatingDay(Date date);

    List<HashMap<String, Object>> judgeGoodNumDay(Date date);

    List<HashMap<String, Object>> courseJudgeNumDay(Date date);

    List<HashMap<String, Object>> courseTopicNumDay(Date date);

    List<HashMap<String, Object>> courseTopicReplyNumDay(Date date);

    List<HashMap<String, Object>> courseNoteOpenNumDay(Date date);

    /**************************************课程统计 每日 end**************************************/

    /**************************************企业统计 start**************************************/

    List<HashMap<String, Object>> countAccountNum();

    List<HashMap<String, Object>> countStudents();

    List<HashMap<String, Object>> countLoginNums();

    List<HashMap<String, Object>> countAccountNouser();

    List<HashMap<String, Object>> countStudyNums();

    List<HashMap<String, Object>> countStudyStuNum();

    List<HashMap<String, Object>> countCourseLearning();

    List<HashMap<String, Object>> countCourseFinished();

    List<HashMap<String, Object>> countStuCourseFinished();

    List<HashMap<String, Object>> countStuExamWait();

    List<HashMap<String, Object>> countStuExamFinished();

    List<HashMap<String, Object>> countStuExamPass();

    List<HashMap<String, Object>> countStuExamNopass();

    List<HashMap<String, Object>> countStuNoteNums();

    List<HashMap<String, Object>> countStuTopicNums();

    List<HashMap<String, Object>> countStuTopicReplyNums();

    List<HashMap<String, Object>> countStuVideoPlays();

    List<HashMap<String, Object>> countStuVideoFinished();

    List<HashMap<String, Object>> countStuCourseViews();

    /**************************************企业统计 end**************************************/

    /**************************************企业日计 end**************************************/

    List<HashMap<String, Object>> countLoginNumsDaily(Date date);

    List<HashMap<String, Object>> courseCreateNumDaily(Date date);

    List<HashMap<String, Object>> countStudyNumsDaily(Date date);

    List<HashMap<String, Object>> countCourseFinishedDaily(Date date);



    List<HashMap<String, Object>> countCourseLearningDaily();

    List<HashMap<String, Object>> countStuCourseFinishedDaily();

    List<HashMap<String, Object>> countStuExamWaitDaily();

    List<HashMap<String, Object>> countStuExamFinishedDaily();

    List<HashMap<String, Object>> countStuExamPassDaily();

    List<HashMap<String, Object>> countStuExamNopassDaily();

    List<HashMap<String, Object>> countStuNoteNumsDaily();

    List<HashMap<String, Object>> countStuTopicNumsDaily();

    List<HashMap<String, Object>> countStuTopicReplyNumsDaily();

    List<HashMap<String, Object>> countStuVideoPlaysDaily();

    List<HashMap<String, Object>> countStuVideoFinishedDaily();

    List<HashMap<String, Object>> countStuCourseViewsDaily();

    List<Integer> selectOrderStatusForOutTime();

    void updateUserOrderByOutTime(Map<String, Object> map);

    /**************************************企业日计 end**************************************/

    /**************************************平台统计 start**************************************/
    HashMap<String, Object> accountNumsAdmin();

    HashMap<String, Object> loginNumsAdmin();

    HashMap<String, Object> studentNumAdmin();

    HashMap<String, Object> orgStudentNumsAdmin();

    HashMap<String, Object> orgNumAdmin();

    HashMap<String, Object> orgRegisterNumAdmin();

    HashMap<String, Object> orgAccountNumsAdmin();

    HashMap<String, Object> orgAccountPaymentAdmin();

    HashMap<String, Object> orgAccountUsedAdmin();

    HashMap<String, Object> courseOrderAdmin();

    HashMap<String, Object> courseOrderPaymentAdmin();

    HashMap<String, Object> orgCourseOrderAdmin();

    HashMap<String, Object> orgCourseOrderPaymentAdmin();

    // HashMap<String, Object> areaNumAdmin();

    HashMap<String, Object> areaAdminNumAdmin();

    HashMap<String, Object> areaAdminLoginAdmin();

    HashMap<String, Object> privateMessageNumsAdmin();

    HashMap<String, Object> announcementNumsAdmin();

    HashMap<String, Object> announcementReadAdmin();

    HashMap<String, Object> noticeNumsAdmin();

    HashMap<String, Object> noticeReadAdmin();

    HashMap<String, Object> privateMessageReadAdmin();

    /**************************************平台统计 end**************************************/

    /**************************************用户数据日计 start**************************************/
    List<HashMap<String, Object>> learningTimeDaily(Date date);

    List<HashMap<String, Object>> courseNumsDaily(Date date);

    List<HashMap<String, Object>> courseFinishedUserDaily(Date date);

    List<HashMap<String, Object>> noteNumsDaily(Date date);

    List<HashMap<String, Object>> noteOpenNumsDaily(Date date);

    List<HashMap<String, Object>> topicNumsDaily(Date date);

    List<HashMap<String, Object>> topicReplyNumsDaily(Date date);

    List<HashMap<String, Object>> courseCollectionDaily(Date date);

    List<HashMap<String, Object>> courseJudgeDaily(Date date);

    List<HashMap<String, Object>> privateMessageDaily(Date date);

    /**************************************用户数据日计 end**************************************/


    /**************************************用户数据统计 start**************************************/
    List<HashMap<String, Object>> learningTime();

    List<HashMap<String, Object>> courseNums();

    List<HashMap<String, Object>> courseFinishedUser();

    List<HashMap<String, Object>> noteNums();

    List<HashMap<String, Object>> noteOpenNums();

    List<HashMap<String, Object>> topicNums();

    List<HashMap<String, Object>> topicReplyNums();

    List<HashMap<String, Object>> courseCollection();

    List<HashMap<String, Object>> courseJudge();

    List<HashMap<String, Object>> privateMessage();


    List<HashMap<String, Object>> signNumsUser();

    List<HashMap<String, Object>> courseLearningUser();

    List<HashMap<String, Object>> examPercentUser();

    List<HashMap<String, Object>> examWaitUser();

    StatisticsUser selectOneByUserId(Integer userId);

    void insertUser(StatisticsUser statisticsUser);

    void updateUser(StatisticsUser statisticsUser);

    /**************************************用户数据统计 end**************************************/

    /**
     * 更新在线用户信息
     */
    void updateUserOnline(UserOnline userOnline);

    UserOnline selectOnlineUser(UserOnline userOnline);

    List<UserOnline> selectOnlineUserList();

    List<HashMap<String, Object>> selectOnline();

    /**************************************用户数据统计 end**************************************/
    List<HashMap<String, Object>> selectEnableMerchantList();

    void updateMerchantStatus(@Param("ids") List<Long> ids);

}
