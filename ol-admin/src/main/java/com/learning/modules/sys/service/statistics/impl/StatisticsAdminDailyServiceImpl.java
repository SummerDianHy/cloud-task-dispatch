package com.learning.modules.sys.service.statistics.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.learning.datasources.DataSourceNames;
import com.learning.datasources.annotation.DataSource;
import com.learning.modules.sys.dao.statistics.StatisticsAdminDailyDao;
import com.learning.modules.sys.entity.statistics.StatisticsAdminDaily;
import com.learning.modules.sys.service.statistics.IStatisticsAdminDailyService;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * create by: JunAo.Yuan
 * description: 平台每日数据统计 实现层
 * create time: 15:19 2018/10/19
 * 
 */
@Service
public class StatisticsAdminDailyServiceImpl extends ServiceImpl<StatisticsAdminDailyDao, StatisticsAdminDaily> implements IStatisticsAdminDailyService {

    /**
     * 强调:
     * 1. 统计相关 读取数据及写入 都需要加上数据源注解, 如此方法
     *
     * @return
     */

    @DataSource(name = DataSourceNames.SECOND)
    @Override
    public void saveAdminDaily(List<HashMap<String, Object>> mapInfo) {
        for (HashMap<String, Object> map : mapInfo) {
            StatisticsAdminDaily adminDaily = new StatisticsAdminDaily();
            Iterator keys = map.keySet().iterator();
            if (map.get("merchantKey") != null) {
                adminDaily.setMerchantKey(map.get("merchantKey").toString());
                adminDaily.setsDate(getSysTime());
                while (keys.hasNext()) {
                    String key = (String) keys.next();
                    if ("loginNumsDaily".equals(key)) {
                        adminDaily.setLoginNums(Integer.parseInt(map.get("loginNumsDaily").toString()));
                    }
                    if ("courseCreate".equals(key)) {
                        adminDaily.setCourseCreate(Integer.parseInt(map.get("courseCreate").toString()));
                    }
                    if ("studyNumsDaily".equals(key)) {
                        adminDaily.setOrgStudentNums(Integer.parseInt(map.get("studyNumsDaily").toString()));
                    }
                    if ("countCourseFinishedDaily".equals(key)) {
                        adminDaily.setOrgNum(Integer.parseInt(map.get("countCourseFinishedDaily").toString()));
                    }
                }
                StatisticsAdminDaily entity = this.selectOne(new EntityWrapper<StatisticsAdminDaily>().eq("merchant_key",map.get("merchant_key")==null?"":map.get("merchant_key").toString())
                        .between("s_date", getStartTime(), getNowEndTime()));
                adminDaily.setUpdateTime(getSysTime());
                if (entity == null) {
                    this.insert(adminDaily);
                } else {
                    this.update(adminDaily, new EntityWrapper<StatisticsAdminDaily>().eq("merchant_key",map.get("merchant_key")==null?"":map.get("merchant_key").toString())
                            .between("s_date", getStartTime(), getNowEndTime()));
                }
            }
        }
    }

    private Date getSysTime() {
        Calendar todayStart = Calendar.getInstance();
        return todayStart.getTime();
    }
    private Date getStartTime() {
        Calendar todayStart = Calendar.getInstance();
        todayStart.set(Calendar.HOUR_OF_DAY, 0);
        todayStart.set(Calendar.MINUTE, 0);
        todayStart.set(Calendar.SECOND, 0);
        todayStart.set(Calendar.MILLISECOND, 0);
        return todayStart.getTime();
    }
    private Date getNowEndTime() {
        Calendar todayEnd = Calendar.getInstance();
        todayEnd.set(Calendar.HOUR_OF_DAY, 23);
        todayEnd.set(Calendar.MINUTE, 59);
        todayEnd.set(Calendar.SECOND, 59);
        todayEnd.set(Calendar.MILLISECOND, 999);
        return todayEnd.getTime();
    }
}
