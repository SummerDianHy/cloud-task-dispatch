package com.learning.modules.sys.service.statistics;

/**
 * <p>
 *     费用统计 接口层
 *
 */
public interface IMerchantCdnLogService {

    /**
     * CDN 统计 保存方法
     * @param
     */
    void saveCDNDaily(String date);

}
