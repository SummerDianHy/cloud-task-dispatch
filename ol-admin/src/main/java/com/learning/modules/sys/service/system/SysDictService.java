package com.learning.modules.sys.service.system;

import com.baomidou.mybatisplus.service.IService;
import com.learning.common.utils.PageUtils;
import com.learning.modules.sys.entity.system.SysDictEntity;

import java.util.Map;


/**
 * <p>
 *
 *    数据字典
 *
 * @description: </p>
 * @author: Kevin
 * @since: Created in 下午8:08 2018/3/31
 */
public interface SysDictService extends IService<SysDictEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

