package com.learning.modules.sys.service.system;


import com.baomidou.mybatisplus.service.IService;
import com.learning.common.utils.PageUtils;
import com.learning.modules.sys.entity.system.SysRoleEntity;

import java.util.Map;

/**
 * <p>
 *
 *    角色管理
 *
 * @description: </p>
 * @author: Kevin
 * @since: Created in 下午8:08 2018/3/31
 */
public interface SysRoleService extends IService<SysRoleEntity> {

	PageUtils queryPage(Map<String, Object> params);

	void save(SysRoleEntity role);

	void update(SysRoleEntity role);
	
	void deleteBatch(Long[] roleIds);

}
