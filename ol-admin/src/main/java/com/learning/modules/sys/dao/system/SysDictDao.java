package com.learning.modules.sys.dao.system;

import com.learning.modules.sys.entity.system.SysDictEntity;
import com.baomidou.mybatisplus.mapper.BaseMapper;


/**
 * <p>
 *
 *    数据字典
 *
 * @description: </p>
 * @author: Kevin
 * @since: Created in 下午8:08 2018/3/31
 */
public interface SysDictDao extends BaseMapper<SysDictEntity> {
	
}
