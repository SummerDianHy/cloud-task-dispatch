package com.learning.modules.sys.entity.statistics;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**费用结算-- 实体层
 * @description: 
 * @author: JunAo.Yuan
 */
@TableName("ol_merchant_fee_log")
public class StatisticsFree implements Serializable {

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;

    /**
     * 主键，自增ID
     */
    @TableId(value = "id",type = IdType.AUTO)
    private Long id;

    /**
     * 商户key
     */
    @TableField(value = "merchant_key")
    private String merchantKey;

    /**
     * 当前费用
     */
    @TableField(value = "before_fee")
    private BigDecimal beforeFee;

    /**
     * 发生费用：-1.20 ；+200.00
     */
    @TableField(value = "fee")
    private BigDecimal fee;

    /**
     * 发生费用后，剩余费用
     */
    @TableField(value = "left_fee")
    private BigDecimal leftFee;

    /**
     * 费用描述；
     */
    @TableField(value = "fee_desc")
    private String feeDesc;

    /**
     * 费用类型0：续费1：升级2：短信3：充值4：CDN 5:直播6：退款
     */
    @TableField(value = "fee_type")
    private Integer feeType;

    /**
     * 发生时间
     */
    @TableField(value = "create_time")
    private Date createTime;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMerchantKey() {
        return merchantKey;
    }

    public void setMerchantKey(String merchantKey) {
        this.merchantKey = merchantKey;
    }

    public BigDecimal getBeforeFee() {
        return beforeFee;
    }

    public void setBeforeFee(BigDecimal beforeFee) {
        this.beforeFee = beforeFee;
    }

    public BigDecimal getFee() {
        return fee;
    }

    public void setFee(BigDecimal fee) {
        this.fee = fee;
    }

    public BigDecimal getLeftFee() {
        return leftFee;
    }

    public void setLeftFee(BigDecimal leftFee) {
        this.leftFee = leftFee;
    }

    public String getFeeDesc() {
        return feeDesc;
    }

    public void setFeeDesc(String feeDesc) {
        this.feeDesc = feeDesc;
    }

    public Integer getFeeType() {
        return feeType;
    }

    public void setFeeType(Integer feeType) {
        this.feeType = feeType;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
}
