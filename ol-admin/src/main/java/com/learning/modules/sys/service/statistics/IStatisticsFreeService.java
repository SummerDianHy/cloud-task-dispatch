package com.learning.modules.sys.service.statistics;

import java.util.HashMap;
import java.util.List;

/**
 * <p>
 *     费用统计 接口层
 *
 */
public interface IStatisticsFreeService {

    /**
     * CDN 统计 保存方法
     * @param courseMap
     */
    void saveFree(List<HashMap<String, Object>> courseMap);

    /**
     * SMS 统计 保存方法
     * @param courseMap
     */
    void saveFreeTwo(List<HashMap<String, Object>> courseMap);


}
