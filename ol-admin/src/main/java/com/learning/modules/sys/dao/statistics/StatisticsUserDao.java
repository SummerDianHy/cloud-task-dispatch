package com.learning.modules.sys.dao.statistics;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.learning.modules.sys.entity.statistics.StatisticsUser;

/**
 * <p>
 *     用户统计 dao
 *
 */
public interface StatisticsUserDao extends BaseMapper<StatisticsUser> {
}
