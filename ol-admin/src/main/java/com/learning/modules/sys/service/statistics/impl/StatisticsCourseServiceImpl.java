package com.learning.modules.sys.service.statistics.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.learning.datasources.DataSourceNames;
import com.learning.datasources.annotation.DataSource;
import com.learning.modules.sys.dao.statistics.StatisticsCourseDao;
import com.learning.modules.sys.entity.statistics.StatisticsCourse;
import com.learning.modules.sys.entity.statistics.StatisticsCourseDaily;
import com.learning.modules.sys.service.base.IPlatFormBaseService;
import com.learning.modules.sys.service.statistics.IStatisticsCourseService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.*;

/**
 * <p>
 * 课程统计实现层
 *
 * @description: </p>
 * @author: Kevin
 * @since: Created in 下午8:55 2018/5/14
 */
@Service
public class StatisticsCourseServiceImpl extends ServiceImpl<StatisticsCourseDao, StatisticsCourse> implements IStatisticsCourseService {

    @Autowired
    IPlatFormBaseService iPlatFormBaseService;

    /**
     * 强调:
     * 1. 统计相关 读取数据及写入 都需要加上数据源注解, 如此方法
     *
     * @return
     */

    @DataSource(name = DataSourceNames.SECOND)
    @Override
    public void saveCourse(List<HashMap<String, Object>> mapInfo) {
        for (HashMap<String, Object> map : mapInfo) {
            StatisticsCourse course = new StatisticsCourse();
            Iterator keys = map.keySet().iterator();
            if (map.get("courseId") != null) {
                course.setCourseId(map.get("courseId").toString());
                course.setCourseCode(map.get("courseCode")==null?"":map.get("courseCode").toString());
                course.setAppKey(map.get("appKey")==null?"":map.get("appKey").toString());
                course.setCourseName(map.get("courseName")==null?"":map.get("courseName").toString());
                course.setCourseCategory(map.get("courseCategory")==null?"":map.get("courseCategory").toString());
                while (keys.hasNext()) {
                    String key = (String) keys.next();
                    if ("favoriteNum".equals(key)) {
                        course.setCollectNum(map.get("favoriteNum")==null?null:Integer.parseInt(map.get("favoriteNum").toString()));
                    }
                    if ("studentNum".equals(key)) {
                        course.setStudentNum(map.get("studentNum")==null?null:Integer.parseInt(map.get("studentNum").toString()));
                    }
                    if ("finishStudy".equals(key)) {
                        course.setFinishedNum(map.get("finishStudy")==null?null:Integer.parseInt(map.get("finishStudy").toString()));
                    }
                    if ("viewNum".equals(key)) {
                        course.setViewsNum(map.get("viewNum")==null?null:Integer.parseInt(map.get("viewNum").toString()));
                    }
                    if ("noteNum".equals(key)) {
                        course.setNoteNum(map.get("noteNum")==null?null:Integer.parseInt(map.get("noteNum").toString()));
                    }
                    if ("courseJudgeNum".equals(key)) {
                        course.setJudgeNum(map.get("courseJudgeNum")==null?null:Integer.parseInt(map.get("courseJudgeNum").toString()));
                    }
                    if ("courseTopicNum".equals(key)) {
                        course.setTopicNum(map.get("courseTopicNum")==null?null:Integer.parseInt(map.get("courseTopicNum").toString()));
                    }
                    if ("courseTopicReplyNum".equals(key)) {
                        course.setTopicReplyNum(map.get("courseTopicReplyNum")==null?null:Integer.parseInt(map.get("courseTopicReplyNum").toString()));
                    }
                    if ("courseNoteOpenNum".equals(key)) {
                        course.setNoteOpenNum(map.get("courseNoteOpenNum")==null?null:Integer.parseInt(map.get("courseNoteOpenNum").toString()));
                    }
                }
                StatisticsCourse entity = this.selectOne(new EntityWrapper<StatisticsCourse>().eq("course_id", map.get("courseId").toString()).eq("app_key",map.get("appKey")==null?"":map.get("appKey").toString()));
                course.setUpdateTime(new Date());
                if (entity == null) {
                    this.insert(course);
                } else {
                    this.update(course, new EntityWrapper<StatisticsCourse>().eq("course_id", entity.getCourseId()).eq("app_key",map.get("appKey")==null?"":map.get("appKey").toString()));
                }
            }
        }
    }
}
