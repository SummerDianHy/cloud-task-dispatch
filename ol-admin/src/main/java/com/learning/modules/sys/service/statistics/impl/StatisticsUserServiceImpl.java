package com.learning.modules.sys.service.statistics.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.learning.datasources.DataSourceNames;
import com.learning.datasources.annotation.DataSource;
import com.learning.modules.sys.dao.statistics.StatisticsUserDailyDao;
import com.learning.modules.sys.dao.statistics.StatisticsUserDao;
import com.learning.modules.sys.entity.statistics.StatisticsUser;
import com.learning.modules.sys.entity.statistics.StatisticsUserDaily;
import com.learning.modules.sys.service.base.IPlatFormBaseService;
import com.learning.modules.sys.service.statistics.IStatisticsUserDailyService;
import com.learning.modules.sys.service.statistics.IStatisticsUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

/**
 * <p>
 * 用户统计实现层
 *
 */
@Service
public class StatisticsUserServiceImpl extends ServiceImpl<StatisticsUserDao, StatisticsUser> implements IStatisticsUserService {

    @Autowired
    IPlatFormBaseService iPlatFormBaseService;

    /**
     * 强调:
     * 1. 统计相关 读取数据及写入 都需要加上数据源注解, 如此方法
     *
     * @return
     */

    @DataSource(name = DataSourceNames.SECOND)
    @Override
    public void saveUser(List<HashMap<String, Object>> mapInfo) {
        for (HashMap<String, Object> map : mapInfo) {
            StatisticsUser user = new StatisticsUser();
            Iterator keys = map.keySet().iterator();
            if (map.get("userId") != null) {
                user.setUserId(Integer.parseInt(map.get("userId").toString()));
                user.setAppKey(map.get("appKey") == null?"":map.get("appKey").toString());
                user.setPhoneNumber(map.get("phoneNumber") == null?"":map.get("phoneNumber").toString());
                user.setUserName(map.get("userName") == null?"":map.get("userName").toString());
                user.setDeptName(map.get("deptName") == null?"":map.get("deptName").toString());
                user.setStationName(map.get("stationName") == null?"":map.get("stationName").toString());
                while (keys.hasNext()) {
                    String key = (String) keys.next();
                    if ("learningTime".equals(key)) {
                        user.setLearningTime(map.get("learningTime")==null?null:Integer.parseInt(map.get("learningTime").toString()));
                    }
                    if ("courseNums".equals(key)) {
                        user.setCourseNums(map.get("courseNums")==null?null:Integer.parseInt(map.get("courseNums").toString()));
                    }
                    if ("courseFinished".equals(key)) {
                        user.setCourseFinished(map.get("courseFinished")==null?null:Integer.parseInt(map.get("courseFinished").toString()));
                    }
                    if ("noteNums".equals(key)) {
                        user.setNoteNums(map.get("noteNums")==null?null:Integer.parseInt(map.get("noteNums").toString()));
                    }
                    if ("noteOpenNums".equals(key)) {
                        user.setNoteOpenNums(map.get("noteOpenNums")==null?null:Integer.parseInt(map.get("noteOpenNums").toString()));
                    }
                    if ("topicNums".equals(key)) {
                        user.setTopicNums(map.get("topicNums")==null?null:Integer.parseInt(map.get("topicNums").toString()));
                    }
                    if ("topicReplyNums".equals(key)) {
                        user.setTopicReplyNums(map.get("topicReplyNums")==null?null:Integer.parseInt(map.get("topicReplyNums").toString()));
                    }
                    if ("courseCollection".equals(key)) {
                        user.setCourseCollection(map.get("courseCollection")==null?null:Integer.parseInt(map.get("courseCollection").toString()));
                    }
                    if ("courseJudge".equals(key)) {
                        user.setCourseJudge(map.get("courseJudge")==null?null:Integer.parseInt(map.get("courseJudge").toString()));
                    }
                    if ("privateMessage".equals(key)) {
                        user.setPrivateMessage(map.get("privateMessage")==null?null:Integer.parseInt(map.get("privateMessage").toString()));
                    }
                }
                StatisticsUser entity = this.selectOne(new EntityWrapper<StatisticsUser>().eq("user_id", map.get("userId").toString()).eq("app_key",map.get("appKey")==null?"":map.get("appKey").toString()));
                user.setUpdateTime(new Date());
                if (entity == null) {
                    this.insert(user);
                } else {
                    this.update(user, new EntityWrapper<StatisticsUser>().eq("user_id", entity.getUserId()));
                }
            }
        }
    }

}
