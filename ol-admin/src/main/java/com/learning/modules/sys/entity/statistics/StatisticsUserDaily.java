package com.learning.modules.sys.entity.statistics;


import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 用户统计报表 --实体层
 *
 * @description:
 * @author: Alvin
 * @Date:20:35 2018/5/14
 */
@TableName("statistics_user_daily")
public class StatisticsUserDaily implements Serializable {

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;

    /**
     * 用户ID,主键
     */
    @TableField(value = "user_id")
    private Integer userId;
    /**
     * 商户app_key
     */
    @TableField(value = "app_key")
    private String appKey;
    /**
     * 统计日期
     */
    @TableField(value = "s_date")
    private Date sDate;

    /**
     * 手机号
     */
    @TableField(value = "phone_number")
    private String phoneNumber;

    /**
     * 姓名
     */
    @TableField(value = "user_name")
    private String userName;

    /**
     * 部门
     */
    @TableField(value = "dept_name")
    private String deptName;
    /**
     * 岗位
     */
    @TableField(value = "station_name")
    private String stationName;


    /**
     * 当日课程数，含购买和分配未学习的
     */
    @TableField(value = "course_nums")
    private Integer courseNums;
    /**
     * 当日学完课程数
     */
    @TableField(value = "course_finished")
    private Integer courseFinished;
    /**
     * 当日学习时长
     */
    @TableField(value = "learning_time")
    private Integer learningTime;

    /**
     * 当日学习笔记数量
     */
    @TableField(value = "note_nums")
    private Integer noteNums;

    /**
     * 当日学习笔记公开数量
     */
    @TableField(value = "note_open_nums")
    private Integer noteOpenNums;

    /**
     * 当日提问数量
     */
    @TableField(value = "topic_nums")
    private Integer topicNums;

    /**
     * 当日回答问题数量
     */
    @TableField(value = "topic_reply_nums")
    private Integer topicReplyNums;

    /**
     * 当日收藏课程数
     */
    @TableField(value = "course_collection")
    private Integer courseCollection;
    /**
     * 当日评价数
     */
    @TableField(value = "course_judge")
    private Integer courseJudge;
    /**
     * 当日私信数
     */
    @TableField(value = "private_message")
    private Integer privateMessage;

    /**
     * 最后更新时间
     */
    @TableField(value = "update_time")
    private Date updateTime;

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Date getsDate() {
        return sDate;
    }

    public void setsDate(Date sDate) {
        this.sDate = sDate;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getDeptName() {
        return deptName;
    }

    public void setDeptName(String deptName) {
        this.deptName = deptName;
    }

    public String getStationName() {
        return stationName;
    }

    public void setStationName(String stationName) {
        this.stationName = stationName;
    }

    public Integer getCourseNums() {
        return courseNums;
    }

    public void setCourseNums(Integer courseNums) {
        this.courseNums = courseNums;
    }

    public Integer getCourseFinished() {
        return courseFinished;
    }

    public void setCourseFinished(Integer courseFinished) {
        this.courseFinished = courseFinished;
    }

    public Integer getLearningTime() {
        return learningTime;
    }

    public void setLearningTime(Integer learningTime) {
        this.learningTime = learningTime;
    }

    public Integer getNoteNums() {
        return noteNums;
    }

    public void setNoteNums(Integer noteNums) {
        this.noteNums = noteNums;
    }

    public Integer getNoteOpenNums() {
        return noteOpenNums;
    }

    public void setNoteOpenNums(Integer noteOpenNums) {
        this.noteOpenNums = noteOpenNums;
    }

    public Integer getTopicNums() {
        return topicNums;
    }

    public void setTopicNums(Integer topicNums) {
        this.topicNums = topicNums;
    }

    public Integer getTopicReplyNums() {
        return topicReplyNums;
    }

    public void setTopicReplyNums(Integer topicReplyNums) {
        this.topicReplyNums = topicReplyNums;
    }

    public Integer getCourseCollection() {
        return courseCollection;
    }

    public void setCourseCollection(Integer courseCollection) {
        this.courseCollection = courseCollection;
    }

    public Integer getCourseJudge() {
        return courseJudge;
    }

    public void setCourseJudge(Integer courseJudge) {
        this.courseJudge = courseJudge;
    }

    public Integer getPrivateMessage() {
        return privateMessage;
    }

    public void setPrivateMessage(Integer privateMessage) {
        this.privateMessage = privateMessage;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public String getAppKey() {
        return appKey;
    }

    public void setAppKey(String appKey) {
        this.appKey = appKey;
    }
}
