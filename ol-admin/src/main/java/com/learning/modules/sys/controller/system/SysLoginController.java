package com.learning.modules.sys.controller.system;


import com.aliyuncs.dysmsapi.model.v20170525.SendSmsResponse;
import com.aliyuncs.exceptions.ClientException;
import com.google.code.kaptcha.Constants;
import com.google.code.kaptcha.Producer;
import com.learning.common.exception.RRException;
import com.learning.common.response.ResponseDataVo;
import com.learning.common.utils.AssertUtil;
import com.learning.common.utils.R;
import com.learning.common.utils.RedisUtils;
import com.learning.common.utils.RegexUtils;
import com.learning.common.utils.alibaba.AliSMSUtils;
import com.learning.modules.sys.shiro.ShiroUtils;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang.StringUtils;
import org.apache.shiro.authc.*;
import org.apache.shiro.subject.Subject;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import org.springframework.beans.factory.annotation.Autowired;
import javax.imageio.ImageIO;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.awt.image.BufferedImage;
import java.io.IOException;

/**
 * <p>
 *
 *    登录相关
 *
 * @description: </p>
 * @author: Kevin
 * @since: Created in 下午8:08 2018/3/31
 */
@Controller
public class SysLoginController {

	@Autowired
	private Producer producer;

	@Autowired
	private RedisUtils redisUtils;
	
	@RequestMapping("captcha.jpg")
	public void captcha(HttpServletResponse response)throws IOException {
        response.setHeader("Cache-Control", "no-store, no-cache");
        response.setContentType("image/jpeg");

        //生成文字验证码
        String text = producer.createText();
        //生成图片验证码
        BufferedImage image = producer.createImage(text);
        //保存到shiro session
        ShiroUtils.setSessionAttribute(Constants.KAPTCHA_SESSION_KEY, text);
        
        ServletOutputStream out = response.getOutputStream();
        ImageIO.write(image, "jpg", out);
	}


	@ApiOperation(value = "发送短信验证码", notes = "发送短信验证码", httpMethod = "GET")
	@RequestMapping(value = "/sendSms", method = RequestMethod.GET)
	@ResponseBody
	public ResponseDataVo<String> sendSms(@RequestParam(required = true) String mobile) {
		AssertUtil.notNull(mobile,"手机号不能为空");
		if (RegexUtils.checkMobile(mobile) == false) {
			throw new RRException("手机号格式不正确");
		}
		try {
			String code = producer.createText();
			SendSmsResponse response = AliSMSUtils.sendSms(mobile, code);
			redisUtils.set(mobile, code, 180);
			return ResponseDataVo.success(response.getCode());
		} catch (ClientException ce) {
			ce.printStackTrace();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return null;
	}
	/**
	 * 登录
	 */
	@ResponseBody
	@RequestMapping(value = "/sys/login", method = RequestMethod.POST)
	public R login(
			@RequestParam(required = true) String username,
			@RequestParam(required = false) String password,
			@RequestParam(required = false) String captcha) {

		if (StringUtils.isNotBlank(captcha)) {
			String kaptcha = ShiroUtils.getKaptcha(Constants.KAPTCHA_SESSION_KEY);
			if(!captcha.equalsIgnoreCase(kaptcha)){
				return R.error("验证码不正确");
			}
		}
		try{
			Subject subject = ShiroUtils.getSubject();
			UsernamePasswordToken token = new UsernamePasswordToken(username, password);
			subject.login(token);
		}catch (UnknownAccountException e) {
			return R.error(e.getMessage());
		}catch (IncorrectCredentialsException e) {
			return R.error("账号或密码不正确");
		}catch (LockedAccountException e) {
			return R.error("账号已被锁定,请联系管理员");
		}catch (AuthenticationException e) {
			return R.error("账户验证失败");
		}
	    
		return R.ok();
	}
	
	/**
	 * 退出
	 */
	@RequestMapping(value = "logout", method = RequestMethod.GET)
	public String logout() {
		ShiroUtils.logout();
		return "redirect:login.html";
	}
	
}
