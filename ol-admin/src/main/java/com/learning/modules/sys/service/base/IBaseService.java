package com.learning.modules.sys.service.base;

import com.learning.modules.sys.entity.statistics.StatisticsUser;
import com.learning.modules.sys.entity.userOnline.UserOnline;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 查询数据公共 层
 *
 * @description: </p>
 * @author: Kevin
 * @since: Created in 下午2:39 2018/5/15
 */
public interface IBaseService {

    /**
     * 获取企业端非本地课程ID
     *
     * @return
     */
    List<String> getAllNotLocalCourseId();

    /**************************************课程统计 start**************************************/

    /**
     * 统计课程销售收入
     *
     * @return
     */
    List<HashMap<String, Object>> countCourseIncome();

    /**
     * 收藏人数
     *
     * @return
     */
    List<HashMap<String, Object>> favoriteNum();

    /**
     * 学员数
     *
     * @return
     */
    List<HashMap<String, Object>> studentNum();

    /**
     * 学习人数
     *
     * @return
     */
    List<HashMap<String, Object>> studyNum();

    /**
     * 完成学员数
     *
     * @return
     */
    List<HashMap<String, Object>> finishStudyNum();

    /**
     * 浏览次数
     *
     * @return
     */
    List<HashMap<String, Object>> viewNum();

    /**
     * 笔记数量
     *
     * @return
     */
    List<HashMap<String, Object>> noteNum();

    /**
     * 课程评论分数
     *
     * @return
     */
    List<HashMap<String, Object>> courseRating();

    /**
     * 课程好评次数
     *
     * @return
     */
    List<HashMap<String, Object>> judgeGoodNum();

    /**
     * 课程评论次数
     *
     * @return
     */
    List<HashMap<String, Object>> courseJudgeNum();

    /**
     * 课程话题数量
     *
     * @return
     */
    List<HashMap<String, Object>> courseTopicNum();

    /**
     * 话题回复数量
     *
     * @return
     */
    List<HashMap<String, Object>> courseTopicReplyNum();

    /**
     * 课程笔记公开数量
     *
     * @return
     */
    List<HashMap<String, Object>> courseNoteOpenNum();

    /**************************************课程统计 end**************************************/

    /**************************************课程统计 每日 start**************************************/

    /**
     * 统计课程销售收入
     *
     * @return
     */
    List<HashMap<String, Object>> countCourseIncomeDay(Date date);

    /**
     * 收藏人数
     *
     * @return
     */
    List<HashMap<String, Object>> favoriteNumDay(Date date);

    /**
     * 学员数
     *
     * @return
     */
    List<HashMap<String, Object>> studentNumDay(Date date);

    /**
     * 学习人数
     *
     * @return
     */
    List<HashMap<String, Object>> studyNumDay(Date date);

    /**
     * 完成学员数
     *
     * @return
     */
    List<HashMap<String, Object>> finishStudyNumDay(Date date);

    /**
     * 浏览次数
     *
     * @return
     */
    List<HashMap<String, Object>> viewNumDay(Date date);

    /**
     * 笔记数量
     *
     * @return
     */
    List<HashMap<String, Object>> noteNumDay(Date date);

    /**
     * 课程评论分数
     *
     * @return
     */
    List<HashMap<String, Object>> courseRatingDay(Date date);

    /**
     * 课程好评次数
     *
     * @return
     */
    List<HashMap<String, Object>> judgeGoodNumDay(Date date);

    /**
     * 课程评论次数
     *
     * @return
     */
    List<HashMap<String, Object>> courseJudgeNumDay(Date date);

    /**
     * 课程话题数量
     *
     * @return
     */
    List<HashMap<String, Object>> courseTopicNumDay(Date date);

    /**
     * 话题回复数量
     *
     * @return
     */
    List<HashMap<String, Object>> courseTopicReplyNumDay(Date date);

    /**
     * 课程笔记公开数量
     *
     * @return
     */
    List<HashMap<String, Object>> courseNoteOpenNumDay(Date date);

    /**************************************课程统计 end**************************************/

    /**************************************企业统计 start**************************************/

    /**
     * 账号数量
     */
    List<HashMap<String, Object>> accountNums();

    /**
     * 学员数量
     */
    List<HashMap<String, Object>> students();

    /**
     * 登录学员数量
     */
    List<HashMap<String, Object>> loginNums();

    /**
     * 未使用帐号数量
     */
    List<HashMap<String, Object>> accountNouser();

    /**
     * 学习课程人数
     */
    List<HashMap<String, Object>> studyNums();

    /**
     * 学习课程人次数
     */
    List<HashMap<String, Object>> studyStuNum();

    /**
     * 学习课程数量
     */
    List<HashMap<String, Object>> courseLearning();

    /**
     * 学完课程数
     */
    List<HashMap<String, Object>> courseFinished();

    /**
     * 学完课程人次数
     */
    List<HashMap<String, Object>> stuCourseFinished();

    /**
     * 待考考试人次数
     */
    List<HashMap<String, Object>> stuExamWait();

    /**
     * 完成考试人次数
     */
    List<HashMap<String, Object>> stuExamFinished();

    /**
     * 考试合格人次数
     */
    List<HashMap<String, Object>> stuExamPass();

    /**
     * 考试不合格人次数
     */
    List<HashMap<String, Object>> stuExamNopass();

    /**
     * 学员笔记数量
     */
    List<HashMap<String, Object>> stuNoteNums();

    /**
     * 学员提问数量
     */
    List<HashMap<String, Object>> stuTopicNums();

    /**
     * 学员回复问题数量
     */
    List<HashMap<String, Object>> stuTopicReplyNums();

    /**
     * 视频播放数量
     */
    List<HashMap<String, Object>> stuVideoPlays();

    /**
     * 看完视频数量
     */
    List<HashMap<String, Object>> stuVideoFinished();

    /**
     * 浏览课程数量
     */
    List<HashMap<String, Object>> stuCourseViews();


    /**************************************企业统计 end**************************************/

    /**************************************企业日计 start**************************************/

    /**
     * 每日登录学员数量
     */
    List<HashMap<String, Object>> countLoginNumsDaily(Date date);


    /**
     * 每日新建课程数
     */
    List<HashMap<String, Object>> courseCreateNumDaily(Date date);


    /**
     * 每日学习人数
     */
    List<HashMap<String, Object>> countStudyNumsDaily(Date date);

    /**
     * 每日完成课程数
     */
    List<HashMap<String, Object>> countCourseFinishedDaily(Date date);

    /**
     * 每日学习课程数量
     */
    List<HashMap<String, Object>> courseLearningDaily();

    /**
     * 每日学完课程数
     */
    List<HashMap<String, Object>> courseFinishedDaily();

    /**
     * 每日学完课程人次数
     */
    List<HashMap<String, Object>> stuCourseFinishedDaily();

    /**
     * 每日待考考试人次数
     */
    List<HashMap<String, Object>> stuExamWaitDaily();

    /**
     * 每日完成考试人次数
     */
    List<HashMap<String, Object>> stuExamFinishedDaily();

    /**
     * 每日考试合格人次数
     */
    List<HashMap<String, Object>> stuExamPassDaily();

    /**
     * 每日考试不合格人次数
     */
    List<HashMap<String, Object>> stuExamNopassDaily();

    /**
     * 每日学员笔记数量
     */
    List<HashMap<String, Object>> stuNoteNumsDaily();

    /**
     * 每日学员提问数量
     */
    List<HashMap<String, Object>> stuTopicNumsDaily();

    /**
     * 每日学员回复问题数量
     */
    List<HashMap<String, Object>> stuTopicReplyNumsDaily();

    /**
     * 每日视频播放数量
     */
    List<HashMap<String, Object>> stuVideoPlaysDaily();

    /**
     * 每日看完视频数量
     */
    List<HashMap<String, Object>> stuVideoFinishedDaily();

    /**
     * 每日浏览课程数量
     */
    List<HashMap<String, Object>> stuCourseViewsDaily();


    /**************************************企业日计 end**************************************/


    /**************************************平台统计 start**************************************/

    HashMap<String, Object> accountNumsAdmin();

    HashMap<String, Object> loginNumsAdmin();

    HashMap<String, Object> studentNumAdmin();

    HashMap<String, Object> orgStudentNumsAdmin();

    HashMap<String, Object> orgNumAdmin();

    HashMap<String, Object> orgRegisterNumAdmin();

    HashMap<String, Object> orgAccountNumsAdmin();

    HashMap<String, Object> orgAccountPaymentAdmin();

    HashMap<String, Object> orgAccountUsedAdmin();

    HashMap<String, Object> courseOrderAdmin();

    HashMap<String, Object> courseOrderPaymentAdmin();

    HashMap<String, Object> orgCourseOrderAdmin();

    HashMap<String, Object> orgCourseOrderPaymentAdmin();

    //HashMap<String, Object> areaNumAdmin();

    HashMap<String, Object> areaAdminNumAdmin();

    HashMap<String, Object> areaAdminLoginAdmin();

    HashMap<String, Object> privateMessageNumsAdmin();

    HashMap<String, Object> privateMessageReadAdmin();

    HashMap<String, Object> announcementNumsAdmin();

    HashMap<String, Object> announcementReadAdmin();

    HashMap<String, Object> noticeNumsAdmin();

    HashMap<String, Object> noticeReadAdmin();

    List<Integer> selectOrderStatusForOutTime();

    void updateUserOrderByOutTime(Map<String, Object> map);

    /**************************************平台统计 end**************************************/

    /**************************************用户统计 每日 start**************************************/
    List<HashMap<String, Object>> courseNumsDaily(Date date);

    List<HashMap<String, Object>> courseFinishedUserDaily(Date date);

    List<HashMap<String, Object>> noteNumsDaily(Date date);

    List<HashMap<String, Object>> noteOpenNumsDaily(Date date);

    List<HashMap<String, Object>> topicNumsDaily(Date date);

    List<HashMap<String, Object>> topicReplyNumsDaily(Date date);

    List<HashMap<String, Object>> learningTimeDaily(Date date);

    List<HashMap<String, Object>> courseCollectionDaily(Date date);

    List<HashMap<String, Object>> courseJudgeDaily(Date date);

    List<HashMap<String, Object>> privateMessageDaily(Date date);

    /**************************************用户统计 end**************************************/

    /**************************************用户数据统计总的 start**************************************/
    List<HashMap<String, Object>> courseNums();

    List<HashMap<String, Object>> courseFinishedUser();

    List<HashMap<String, Object>> noteNums();

    List<HashMap<String, Object>> noteOpenNums();

    List<HashMap<String, Object>> topicNums();

    List<HashMap<String, Object>> topicReplyNums();

    List<HashMap<String, Object>> learningTime();

    List<HashMap<String, Object>> courseCollection();

    List<HashMap<String, Object>> courseJudge();

    List<HashMap<String, Object>> privateMessage();


    List<HashMap<String, Object>> signNumsUser();

    List<HashMap<String, Object>> courseLearningUser();

    List<HashMap<String, Object>> examPercentUser();

    List<HashMap<String, Object>> examWaitUser();

    StatisticsUser selectOneByUserId(Integer userId);

    void insertUser(StatisticsUser user);

    void updateUser(StatisticsUser user);

    /**************************************用户数据统计 end**************************************/

    void updateUserOnline(UserOnline userOnline);

    List<UserOnline> selectOnlineUserList();

    List<HashMap<String, Object>> selectOnline();

    /**************************************商户数据统计 end**************************************/
    List<HashMap<String, Object>> selectEnableMerchantList();

    void updateMerchantStatus(List<Long> list);
}
