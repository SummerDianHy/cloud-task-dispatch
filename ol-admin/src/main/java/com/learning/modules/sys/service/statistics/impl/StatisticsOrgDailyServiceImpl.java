package com.learning.modules.sys.service.statistics.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.learning.datasources.DataSourceNames;
import com.learning.datasources.annotation.DataSource;
import com.learning.modules.sys.dao.statistics.StatisticsOrgDailyDao;
import com.learning.modules.sys.entity.statistics.StatisticsOrgDaily;
import com.learning.modules.sys.service.statistics.IStatisticsOrgDailyService;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * create by: JunAo.Yuan
 * description: 企业每日数据统计 实现层
 * create time: 15:19 2018/10/19
 * 
 */
@Service
public class StatisticsOrgDailyServiceImpl extends ServiceImpl<StatisticsOrgDailyDao, StatisticsOrgDaily> implements IStatisticsOrgDailyService {

    /**
     * 强调:
     * 1. 统计相关 读取数据及写入 都需要加上数据源注解, 如此方法
     *
     * @return
     */

    @DataSource(name = DataSourceNames.SECOND)
    @Override
    public void saveOrgDaily(List<HashMap<String, Object>> mapInfo) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        for (HashMap<String, Object> map : mapInfo) {
            StatisticsOrgDaily orgDaily = new StatisticsOrgDaily();
            Iterator keys = map.keySet().iterator();
            if (map.get("merchantKey") != null) {
                orgDaily.setMerchantKey(map.get("merchantKey").toString());
                try {
                    orgDaily.setsDate(sdf.parse(map.get("sDate").toString()));
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                while (keys.hasNext()) {
                    String key = (String) keys.next();
                    if ("loginNumsDaily".equals(key)) {
                        orgDaily.setLoginNums(Integer.parseInt(map.get("loginNumsDaily").toString()));
                    }
                    if ("courseCreate".equals(key)) {
                        orgDaily.setCourseCreate(Integer.parseInt(map.get("courseCreate").toString()));
                    }
                    if ("studyNumsDaily".equals(key)) {
                        orgDaily.setStudyStuNum(Integer.parseInt(map.get("studyNumsDaily").toString()));
                    }
                    if ("studyFinishNumsDaily".equals(key)) {
                        orgDaily.setCourseFinished(Integer.parseInt(map.get("studyFinishNumsDaily").toString()));
                    }
                }
                StatisticsOrgDaily entity = null;
                try {
                    entity = this.selectOne(new EntityWrapper<StatisticsOrgDaily>().eq("merchant_key",orgDaily.getMerchantKey())
                            .eq("s_date",sdf.parse(map.get("sDate").toString())));
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                orgDaily.setUpdateTime(getSysTime());
                if (entity == null) {
                    this.insert(orgDaily);
                } else {
                    try {
                        this.update(orgDaily, new EntityWrapper<StatisticsOrgDaily>().eq("merchant_key",orgDaily.getMerchantKey())
                                .eq("s_date",sdf.parse(map.get("sDate").toString())));
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    private Date getSysTime() {
        Calendar todayStart = Calendar.getInstance();
        return todayStart.getTime();
    }
    private Date getStartTime() {
        Calendar todayStart = Calendar.getInstance();
        todayStart.set(Calendar.HOUR_OF_DAY, 0);
        todayStart.set(Calendar.MINUTE, 0);
        todayStart.set(Calendar.SECOND, 0);
        todayStart.set(Calendar.MILLISECOND, 0);
        return todayStart.getTime();
    }
    private Date getNowEndTime() {
        Calendar todayEnd = Calendar.getInstance();
        todayEnd.set(Calendar.HOUR_OF_DAY, 23);
        todayEnd.set(Calendar.MINUTE, 59);
        todayEnd.set(Calendar.SECOND, 59);
        todayEnd.set(Calendar.MILLISECOND, 999);
        return todayEnd.getTime();
    }
}

