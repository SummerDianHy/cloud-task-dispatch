package com.learning.modules.sys.entity.statistics;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 企业每日数据统计 --实体层
 * @description:
 * @author: Alvin
 * @Date:20:18 2018/5/14
 */
@TableName("statistics_org_daily")
public class StatisticsOrgDaily  implements Serializable {

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;

    /**
     * 企业app_key
     */
    @TableField(value = "merchant_key")
    private String merchantKey;

    /**
     * 统计日期
     */
    @TableField(value = "s_date")
    private Date sDate;

    /**
     * 当日增加课程数量
     */
    @TableField(value = "course_create")
    private Integer courseCreate;

    /**
     * 当日增加帐号数量
     */
    @TableField(value = "account_nums")
    private Integer accountNums;

    /**
     * 当日支付金额
     */
    @TableField(value = "payment")
    private BigDecimal payment;

    /**
     * 当日登录学员数量
     */
    @TableField(value = "login_nums")
    private Integer loginNums;

    /**
     * 当日学习课程人数
     */
    @TableField(value = "study_num")
    private Integer studyNum;

    /**
     * 当日学习课程人次数
     */
    @TableField(value = "study_stu_num")
    private Integer studyStuNum;

    /**
     * 当日学习中课程数量
     */
    @TableField(value = "course_learning")
    private Integer courseLearning;

    /**
     * 当日学完课程数
     */
    @TableField(value = "course_finished")
    private Integer courseFinished;

    /**
     * 当日学完课程人次数
     */
    @TableField(value = "stu_course_finished")
    private Integer stuCourseFinished;

    /**
     * 当日待考考试人次数
     */
    @TableField(value = "stu_exam_wait")
    private Integer stuExamWait;

    /**
     * 当日完成考试人次数
     */
    @TableField(value = "stu_exam_finished")
    private Integer stuExamFinished;

    /**
     * 当日考试合格人次数
     */
    @TableField(value = "stu_exam_pass")
    private Integer stuExamPass;

    /**
     * 当日考试不合格人次数
     */
    @TableField(value = "stu_exam_nopass")
    private Integer stuExamNopass;

    /**
     * 当日学员笔记数量
     */
    @TableField(value = "stu_note_nums")
    private Integer stuNoteNums;

    /**
     * 当日学员提问数量
     */
    @TableField(value = "stu_topic_nums")
    private Integer stuTopicNums;

    /**
     * 当日学员回复问题数量
     */
    @TableField(value = "stu_topic_reply_nums")
    private Integer stuTopicReplyNums;

    /**
     * 当日视频播放数量
     */
    @TableField(value = "stu_video_plays")
    private Integer stuVideoPlays;

    /**
     * 当日看完视频数量
     */
    @TableField(value = "stu_video_finished")
    private Integer stuVideoFinished;

    /**
     * 当日浏览课程数
     */
    @TableField(value = "stu_course_views")
    private Integer stuCourseViews;

    /**
     * 最后统计时间
     */
    @TableField(value = "update_time")
    private Date updateTime;

    public String getMerchantKey() {
        return merchantKey;
    }

    public void setMerchantKey(String merchantKey) {
        this.merchantKey = merchantKey;
    }

    public Integer getCourseCreate() {
        return courseCreate;
    }

    public void setCourseCreate(Integer courseCreate) {
        this.courseCreate = courseCreate;
    }

    public Date getsDate() {
        return sDate;
    }

    public void setsDate(Date sDate) {
        this.sDate = sDate;
    }

    public Integer getAccountNums() {
        return accountNums;
    }

    public void setAccountNums(Integer accountNums) {
        this.accountNums = accountNums;
    }

    public BigDecimal getPayment() {
        return payment;
    }

    public void setPayment(BigDecimal payment) {
        this.payment = payment;
    }

    public Integer getLoginNums() {
        return loginNums;
    }

    public void setLoginNums(Integer loginNums) {
        this.loginNums = loginNums;
    }

    public Integer getStudyNum() {
        return studyNum;
    }

    public void setStudyNum(Integer studyNum) {
        this.studyNum = studyNum;
    }

    public Integer getStudyStuNum() {
        return studyStuNum;
    }

    public void setStudyStuNum(Integer studyStuNum) {
        this.studyStuNum = studyStuNum;
    }

    public Integer getCourseLearning() {
        return courseLearning;
    }

    public void setCourseLearning(Integer courseLearning) {
        this.courseLearning = courseLearning;
    }

    public Integer getCourseFinished() {
        return courseFinished;
    }

    public void setCourseFinished(Integer courseFinished) {
        this.courseFinished = courseFinished;
    }

    public Integer getStuCourseFinished() {
        return stuCourseFinished;
    }

    public void setStuCourseFinished(Integer stuCourseFinished) {
        this.stuCourseFinished = stuCourseFinished;
    }

    public Integer getStuExamWait() {
        return stuExamWait;
    }

    public void setStuExamWait(Integer stuExamWait) {
        this.stuExamWait = stuExamWait;
    }

    public Integer getStuExamFinished() {
        return stuExamFinished;
    }

    public void setStuExamFinished(Integer stuExamFinished) {
        this.stuExamFinished = stuExamFinished;
    }

    public Integer getStuExamPass() {
        return stuExamPass;
    }

    public void setStuExamPass(Integer stuExamPass) {
        this.stuExamPass = stuExamPass;
    }

    public Integer getStuExamNopass() {
        return stuExamNopass;
    }

    public void setStuExamNopass(Integer stuExamNopass) {
        this.stuExamNopass = stuExamNopass;
    }

    public Integer getStuNoteNums() {
        return stuNoteNums;
    }

    public void setStuNoteNums(Integer stuNoteNums) {
        this.stuNoteNums = stuNoteNums;
    }

    public Integer getStuTopicNums() {
        return stuTopicNums;
    }

    public void setStuTopicNums(Integer stuTopicNums) {
        this.stuTopicNums = stuTopicNums;
    }

    public Integer getStuTopicReplyNums() {
        return stuTopicReplyNums;
    }

    public void setStuTopicReplyNums(Integer stuTopicReplyNums) {
        this.stuTopicReplyNums = stuTopicReplyNums;
    }

    public Integer getStuVideoPlays() {
        return stuVideoPlays;
    }

    public void setStuVideoPlays(Integer stuVideoPlays) {
        this.stuVideoPlays = stuVideoPlays;
    }

    public Integer getStuVideoFinished() {
        return stuVideoFinished;
    }

    public void setStuVideoFinished(Integer stuVideoFinished) {
        this.stuVideoFinished = stuVideoFinished;
    }

    public Integer getStuCourseViews() {
        return stuCourseViews;
    }

    public void setStuCourseViews(Integer stuCourseViews) {
        this.stuCourseViews = stuCourseViews;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

}
