package com.learning.modules.sys.entity.statistics;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**课程统计-- 实体层
 * @description: 
 * @author: Alvin
 * @Date:19:59 2018/5/14
 */
@TableName("statistics_course")
public class StatisticsCourse implements Serializable {

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;

    /**
     * 课程id
     */
    @TableField(value = "course_id")
    private String courseId;

    /**
     * 课程code
     */
    @TableField(value = "course_code")
    private String courseCode;

    /**
     * 企业app_key
     */
    @TableField(value = "app_key")
    private String appKey;

    /**
     * 课程名称
     */
    @TableField(value = "course_name")
    private String courseName;

    /**
     * 课程分类
     */
    @TableField(value = "course_category")
    private String courseCategory;

    /**
     * 课程销售总收入
     */
    @TableField(value = "income")
    private BigDecimal income;

    /**
     * 排行分数
     */
    @TableField(value = "rating")
    private Integer rating;

    /**
     * 收藏人数
     */
    @TableField(value = "collect_num")
    private Integer collectNum;

    /**
     * 总学员数
     */
    @TableField(value = "student_num")
    private Integer studentNum;

    /**
     * 学习学员数
     */
    @TableField(value = "study_num")
    private Integer studyNum;

    /**
     * 完成课程学员数量
     */
    @TableField(value = "finished_num")
    private Integer finishedNum;

    /**
     * 查看浏览次数
     */
    @TableField(value = "views_num")
    private Integer viewsNum;

    /**
     * 课程笔记数量
     */
    @TableField(value = "note_num")
    private Integer noteNum;

    /**
     * 评价次数
     */
    @TableField(value = "judge_num")
    private Integer judgeNum;

    /**
     * 好评次数
     */
    @TableField(value = "judge_good_num")
    private Integer judgeGoodNum;

    /**
     * 话题数量
     */
    @TableField(value = "topic_num")
    private Integer topicNum;

    /**
     * 话题回复数量
     */
    @TableField(value = "topic_reply_num")
    private Integer topicReplyNum;

    /**
     * 好评次数
     */
    @TableField(value = "note_open_num")
    private Integer noteOpenNum;

    /**
     * 最后统计时间
     */
    @TableField(value = "update_time")
    private Date updateTime;

    public String getCourseId() {
        return courseId;
    }

    public void setCourseId(String courseId) {
        this.courseId = courseId;
    }

    public BigDecimal getIncome() {
        return income;
    }

    public void setIncome(BigDecimal income) {
        this.income = income;
    }

    public Integer getRating() {
        return rating;
    }

    public void setRating(Integer rating) {
        this.rating = rating;
    }

    public Integer getCollectNum() {
        return collectNum;
    }

    public void setCollectNum(Integer collectNum) {
        this.collectNum = collectNum;
    }

    public Integer getStudentNum() {
        return studentNum;
    }

    public void setStudentNum(Integer studentNum) {
        this.studentNum = studentNum;
    }

    public Integer getStudyNum() {
        return studyNum;
    }

    public void setStudyNum(Integer studyNum) {
        this.studyNum = studyNum;
    }

    public Integer getFinishedNum() {
        return finishedNum;
    }

    public void setFinishedNum(Integer finishedNum) {
        this.finishedNum = finishedNum;
    }

    public Integer getViewsNum() {
        return viewsNum;
    }

    public void setViewsNum(Integer viewsNum) {
        this.viewsNum = viewsNum;
    }

    public Integer getNoteNum() {
        return noteNum;
    }

    public void setNoteNum(Integer noteNum) {
        this.noteNum = noteNum;
    }

    public Integer getJudgeNum() {
        return judgeNum;
    }

    public void setJudgeNum(Integer judgeNum) {
        this.judgeNum = judgeNum;
    }

    public Integer getJudgeGoodNum() {
        return judgeGoodNum;
    }

    public void setJudgeGoodNum(Integer judgeGoodNum) {
        this.judgeGoodNum = judgeGoodNum;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public String getCourseCode() {
        return courseCode;
    }

    public void setCourseCode(String courseCode) {
        this.courseCode = courseCode;
    }

    public String getCourseName() {
        return courseName;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    public String getCourseCategory() {
        return courseCategory;
    }

    public void setCourseCategory(String courseCategory) {
        this.courseCategory = courseCategory;
    }

    public Integer getTopicNum() {
        return topicNum;
    }

    public void setTopicNum(Integer topicNum) {
        this.topicNum = topicNum;
    }

    public Integer getTopicReplyNum() {
        return topicReplyNum;
    }

    public void setTopicReplyNum(Integer topicReplyNum) {
        this.topicReplyNum = topicReplyNum;
    }

    public Integer getNoteOpenNum() {
        return noteOpenNum;
    }

    public void setNoteOpenNum(Integer noteOpenNum) {
        this.noteOpenNum = noteOpenNum;
    }

    public String getAppKey() {
        return appKey;
    }

    public void setAppKey(String appKey) {
        this.appKey = appKey;
    }
}
