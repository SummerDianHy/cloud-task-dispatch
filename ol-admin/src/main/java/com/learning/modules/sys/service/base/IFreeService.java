package com.learning.modules.sys.service.base;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

/**
 * 查询费用
 */
public interface IFreeService {

    //更改商户余额
    void updateMerchantFree(String merchantKey, BigDecimal free);

    //获取所有商户
    List<HashMap<String, Object>> getMerchantInfo();

    //统计CDN费用日志
    List<HashMap<String, Object>> getCDNFreeRecord();

    //统计SMS费用日志
    List<HashMap<String, Object>> getSMSFreeRecord();

    //查询CDN统计日期
    List<HashMap<String, Date>> getCDNFreeRecordGroupByDay();


}
