package com.learning.modules.sys.service.statistics;

import java.util.HashMap;
import java.util.List;

/**
 * create by: JunAo.Yuan
 * description:平台每日数据统计  接口层
 * create time: 14:59 2018/10/19
 *
 */
public interface IStatisticsAdminDailyService {

    /**
     * 平台每日数据统计 保存方法
     * @param adminDailyMap
     */
    void saveAdminDaily(List<HashMap<String, Object>> adminDailyMap);

}
