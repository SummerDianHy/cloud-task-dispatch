package com.learning.modules.sys.service.statistics.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.learning.common.utils.DateUtils;
import com.learning.datasources.DataSourceNames;
import com.learning.datasources.annotation.DataSource;
import com.learning.modules.sys.dao.base.BaseDao;
import com.learning.modules.sys.dao.base.PlatFormBaseDao;
import com.learning.modules.sys.dao.statistics.UserOnlineDao;
import com.learning.modules.sys.entity.userOnline.UserOnline;
import com.learning.modules.sys.service.statistics.IUserOnlineService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

/**
 * <p>
 * 用户在线统计实现层
 *
 */
@Service
public class UserOnlineServiceImpl extends ServiceImpl<UserOnlineDao, UserOnline> implements IUserOnlineService {

    @Autowired
    BaseDao baseDao;

    @Autowired
    PlatFormBaseDao platFormBaseDao;
    /**
     * 强调:
     * 1. 统计相关 读取数据及写入 都需要加上数据源注解, 如此方法
     *
     * @return
     */

    @DataSource(name = DataSourceNames.SECOND)
    @Override
    public void updateUserOnline(List<UserOnline> userOnlineList) {
        List<UserOnline> userOnlines = new ArrayList<>();
        for (UserOnline userOnline : userOnlineList) {
            Long s = (System.currentTimeMillis() - userOnline.getOnlineTime().getTime()) / (1000 * 60);
            if (s.intValue() >= 10) {
                userOnline.setIsOnline(0);//0：不在线，1：在线
                userOnlines.add(userOnline);
            }
        }
        if (userOnlines.size() > 0) {
            this.updateBatchById(userOnlines);
        }
    }
    /**
     * 强调:
     * 1. 统计相关 读取数据及写入 都需要加上数据源注解, 如此方法
     *
     * @return
     */

    @DataSource(name = DataSourceNames.SECOND)
    @Override
    public void updateMerchantStatus(List<HashMap<String, Object>> list) {
        List<Long> ids = new ArrayList<>();
        if (list != null && list.size() > 0) {
            for (HashMap<String, Object> map : list) {
                Long merchantId = Long.parseLong(map.get("merchantId").toString());
                Date orgDeadline = (Date) map.get("orgDeadline");
                if (System.currentTimeMillis() > orgDeadline.getTime()) {
                    ids.add(merchantId);
                }
            }
        }
        if (ids.size() > 0) {
            baseDao.updateMerchantStatus(ids);
        }
    }
    /**
     * 强调:
     * 1. 统计相关 读取数据及写入 都需要加上数据源注解, 如此方法
     *
     * @return
     */

    @DataSource(name = DataSourceNames.THIRD)
    @Override
    public void updateMerchantStatusTwo(List<HashMap<String, Object>> list) {
        List<Long> ids = new ArrayList<>();
        if (list != null && list.size() > 0) {
            for (HashMap<String, Object> map : list) {
                Long merchantId = Long.parseLong(map.get("merchantId").toString());
                Date orgDeadline = (Date) map.get("orgDeadline");
                if (System.currentTimeMillis() > orgDeadline.getTime()) {
                    ids.add(merchantId);
                }
            }
        }
        if (ids.size() > 0) {
            platFormBaseDao.updateMerchantStatus(ids);
        }
    }

}
