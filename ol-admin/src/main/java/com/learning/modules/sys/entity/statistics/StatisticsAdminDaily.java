package com.learning.modules.sys.entity.statistics;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 课程统计-- 实体层
 *
 * @description:
 * @author: Alvin
 * @Date:19:59 2018/5/14
 */
@TableName("statistics_admin_daily")
public class StatisticsAdminDaily implements Serializable {

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;

    /**
     * 统计日期
     */
    @TableField(value = "s_date")
    private Date sDate;


    /**
     * 当日新增课程数量
     */
    @TableField(value = "course_create")
    private Integer courseCreate;


    /**
     * 商戶app_key
     */
    @TableField(value = "merchant_key")
    private String merchantKey;

    /**
     * 当日登录学员数量
     */
    @TableField(value = "login_nums")
    private Integer loginNums;

    /**
     * 当日新增学员数量
     */
    @TableField(value = "student_nums")
    private Integer studentNum;

    /**
     * 当日新增企业学员数量
     */
    @TableField(value = "org_student_nums")
    private Integer orgStudentNums;

    /**
     * 当日新增企业数量
     */
    @TableField(value = "org_num")
    private Integer orgNum;

    /**
     * 当日自己注册企业数量
     */
    @TableField(value = "org_register_num")
    private Integer orgRegisterNum;

    /**
     * 当日新增企业帐号数量
     */
    @TableField(value = "org_account_nums")
    private Integer orgAccountNums;

    /**
     * 当日新增企业帐号订单金额
     */
    @TableField(value = "org_account_payment")
    private BigDecimal orgAccountPayment;

    /**
     * 当日新增企业帐号使用数量
     */
    @TableField(value = "org_account_used")
    private Integer orgAccountUsed;

    /**
     * 当日新增课程订单数量
     */
    @TableField(value = "course_order")
    private Integer courseOrder;

    /**
     * 当日新增课程订单金额
     */
    @TableField(value = "course_order_payment")
    private BigDecimal courseOrderPayment;

    /**
     * 当日新增课程订单数量(企业学员)
     */
    @TableField(value = "org_course_order")
    private Integer orgCourseOrder;

    /**
     * 当日新增课程订单金额(企业学员)
     */
    @TableField(value = "org_course_order_payment")
    private BigDecimal orgCourseOrderPayment;

    /**
     * 当日新增区域数
     */
    @TableField(value = "area_num")
    private Integer areaNum;

    /**
     * 当日新增区域管理员数
     */
    @TableField(value = "area_admin_num")
    private Integer areaAdminNum;

    /**
     * 当日新增区域管理登录数量
     */
    @TableField(value = "area_admin_login")
    private Integer areaAdminLogin;

    /**
     * 当日私信数量
     */
    @TableField(value = "private_message_nums")
    private Integer privateMessageNums;

    /**
     * 当日私信阅读数量
     */
    @TableField(value = "private_message_read")
    private Integer privateMessageRead;

    /**
     * 当日公告数量
     */
    @TableField(value = "announcement_nums")
    private Integer announcementNums;

    /**
     * 当日公告阅读数量
     */
    @TableField(value = "announcement_read")
    private Integer announcementRead;

    /**
     * 当日通知数量
     */
    @TableField(value = "notice_nums")
    private Integer noticeNums;

    /**
     * 当日通知阅读数量
     */
    @TableField(value = "notice_read")
    private Integer noticeRead;

    /**
     * 最后统计时间
     */
    @TableField(value = "update_time")
    private Date updateTime;


    public Date getsDate() {
        return sDate;
    }

    public void setsDate(Date sDate) {
        this.sDate = sDate;
    }

    public Integer getCourseCreate() {
        return courseCreate;
    }

    public void setCourseCreate(Integer courseCreate) {
        this.courseCreate = courseCreate;
    }

    public Integer getLoginNums() {
        return loginNums;
    }

    public void setLoginNums(Integer loginNums) {
        this.loginNums = loginNums;
    }

    public Integer getStudentNum() {
        return studentNum;
    }

    public void setStudentNum(Integer studentNum) {
        this.studentNum = studentNum;
    }

    public Integer getOrgStudentNums() {
        return orgStudentNums;
    }

    public void setOrgStudentNums(Integer orgStudentNums) {
        this.orgStudentNums = orgStudentNums;
    }

    public Integer getOrgNum() {
        return orgNum;
    }

    public void setOrgNum(Integer orgNum) {
        this.orgNum = orgNum;
    }

    public Integer getOrgRegisterNum() {
        return orgRegisterNum;
    }

    public void setOrgRegisterNum(Integer orgRegisterNum) {
        this.orgRegisterNum = orgRegisterNum;
    }

    public Integer getOrgAccountNums() {
        return orgAccountNums;
    }

    public void setOrgAccountNums(Integer orgAccountNums) {
        this.orgAccountNums = orgAccountNums;
    }

    public BigDecimal getOrgAccountPayment() {
        return orgAccountPayment;
    }

    public void setOrgAccountPayment(BigDecimal orgAccountPayment) {
        this.orgAccountPayment = orgAccountPayment;
    }

    public Integer getOrgAccountUsed() {
        return orgAccountUsed;
    }

    public void setOrgAccountUsed(Integer orgAccountUsed) {
        this.orgAccountUsed = orgAccountUsed;
    }

    public Integer getCourseOrder() {
        return courseOrder;
    }

    public void setCourseOrder(Integer courseOrder) {
        this.courseOrder = courseOrder;
    }

    public BigDecimal getCourseOrderPayment() {
        return courseOrderPayment;
    }

    public void setCourseOrderPayment(BigDecimal courseOrderPayment) {
        this.courseOrderPayment = courseOrderPayment;
    }

    public Integer getOrgCourseOrder() {
        return orgCourseOrder;
    }

    public void setOrgCourseOrder(Integer orgCourseOrder) {
        this.orgCourseOrder = orgCourseOrder;
    }

    public BigDecimal getOrgCourseOrderPayment() {
        return orgCourseOrderPayment;
    }

    public void setOrgCourseOrderPayment(BigDecimal orgCourseOrderPayment) {
        this.orgCourseOrderPayment = orgCourseOrderPayment;
    }

    public Integer getAreaNum() {
        return areaNum;
    }

    public void setAreaNum(Integer areaNum) {
        this.areaNum = areaNum;
    }

    public Integer getAreaAdminNum() {
        return areaAdminNum;
    }

    public void setAreaAdminNum(Integer areaAdminNum) {
        this.areaAdminNum = areaAdminNum;
    }

    public Integer getAreaAdminLogin() {
        return areaAdminLogin;
    }

    public void setAreaAdminLogin(Integer areaAdminLogin) {
        this.areaAdminLogin = areaAdminLogin;
    }

    public Integer getPrivateMessageNums() {
        return privateMessageNums;
    }

    public void setPrivateMessageNums(Integer privateMessageNums) {
        this.privateMessageNums = privateMessageNums;
    }

    public Integer getPrivateMessageRead() {
        return privateMessageRead;
    }

    public void setPrivateMessageRead(Integer privateMessageRead) {
        this.privateMessageRead = privateMessageRead;
    }

    public Integer getAnnouncementNums() {
        return announcementNums;
    }

    public void setAnnouncementNums(Integer announcementNums) {
        this.announcementNums = announcementNums;
    }

    public Integer getAnnouncementRead() {
        return announcementRead;
    }

    public void setAnnouncementRead(Integer announcementRead) {
        this.announcementRead = announcementRead;
    }

    public Integer getNoticeNums() {
        return noticeNums;
    }

    public void setNoticeNums(Integer noticeNums) {
        this.noticeNums = noticeNums;
    }

    public Integer getNoticeRead() {
        return noticeRead;
    }

    public void setNoticeRead(Integer noticeRead) {
        this.noticeRead = noticeRead;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public String getMerchantKey() {
        return merchantKey;
    }

    public void setMerchantKey(String merchantKey) {
        this.merchantKey = merchantKey;
    }
}
