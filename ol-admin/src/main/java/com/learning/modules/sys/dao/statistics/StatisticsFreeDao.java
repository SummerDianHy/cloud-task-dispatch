package com.learning.modules.sys.dao.statistics;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.learning.modules.sys.entity.statistics.StatisticsCourse;
import com.learning.modules.sys.entity.statistics.StatisticsFree;

/**
 * <p>
 *     费用结算 dao
 *
 */
public interface StatisticsFreeDao extends BaseMapper<StatisticsFree> {
}
