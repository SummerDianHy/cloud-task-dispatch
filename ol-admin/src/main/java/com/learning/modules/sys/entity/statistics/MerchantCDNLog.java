package com.learning.modules.sys.entity.statistics;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**CDN费用记录-- 实体层
 * @description: 
 * @author: JunAo.Yuan
 */
@TableName("ol_merchant_cdn_log")
public class MerchantCDNLog implements Serializable {

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;

    /**
     * 主键，自增ID
     */
    @TableId(value = "id",type = IdType.AUTO)
    private Long id;

    /**
     * 商户key
     */
    @TableField(value = "merchant_key")
    private String merchantKey;

    /**
     * CND加速域名
     */
    @TableField(value = "domain_name")
    private String domainName;

    /**
     * 当前使用CDN域名
     */
    @TableField(value = "referer")
    private String referer;

    /**
     * 访问次数
     */
    @TableField(value = "visitdata")
    private String visitdata;

    /**
     * 流量。单位：byte
     */
    @TableField(value = "flow")
    private String flow;

    /**
     * 记录时间
     */
    @TableField(value = "find_time")
    private Date findTime;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMerchantKey() {
        return merchantKey;
    }

    public void setMerchantKey(String merchantKey) {
        this.merchantKey = merchantKey;
    }

    public String getDomainName() {
        return domainName;
    }

    public void setDomainName(String domainName) {
        this.domainName = domainName;
    }

    public String getReferer() {
        return referer;
    }

    public void setReferer(String referer) {
        this.referer = referer;
    }

    public String getVisitdata() {
        return visitdata;
    }

    public void setVisitdata(String visitdata) {
        this.visitdata = visitdata;
    }

    public String getFlow() {
        return flow;
    }

    public void setFlow(String flow) {
        this.flow = flow;
    }

    public Date getFindTime() {
        return findTime;
    }

    public void setFindTime(Date findTime) {
        this.findTime = findTime;
    }
}
