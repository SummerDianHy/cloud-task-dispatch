package com.learning.modules.sys.service.base.impl;

import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;
import com.learning.datasources.DataSourceNames;
import com.learning.datasources.annotation.DataSource;
import com.learning.modules.sys.dao.base.FreeDao;
import com.learning.modules.sys.dao.statistics.MerchantSMSLogFreeDao;
import com.learning.modules.sys.entity.statistics.MerchantSmsLog;
import com.learning.modules.sys.service.base.IFreeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

/**
 * 费用计算 实现层
 */
@Service
public class FreeServiceImpl implements IFreeService {

    @Autowired
    private FreeDao freeDao;



    /**
     * 获取所有可用商户（主要获取商户appKey和merchantDomain）
     *
     * @return
     */
    @DataSource(name = DataSourceNames.THIRD)
    @Override
    public List<HashMap<String, Object>> getMerchantInfo() {
        return freeDao.getMerchantInfo();
    }

    /**
     * 统计CDN
     *
     * @return
     */
    @DataSource(name = DataSourceNames.THIRD)
    @Override
    public List<HashMap<String, Object>> getCDNFreeRecord() {

        return freeDao.getCDNFreeRecord();
    }
    /**
     * 统计SMS
     *
     * @return
     */
    @DataSource(name = DataSourceNames.THIRD)
    @Override
    public List<HashMap<String, Object>> getSMSFreeRecord() {
        return freeDao.getSMSFreeRecord();
    }
    /**
     * 查询CDN统计日期
     *
     * @return
     */
    @DataSource(name = DataSourceNames.THIRD)
    @Override
    public List<HashMap<String, Date>> getCDNFreeRecordGroupByDay() {
        return freeDao.getCDNFreeRecordGroupByDay();
    }
    /**
     * 更改商户余额
     *
     * @return
     */
    @DataSource(name = DataSourceNames.THIRD)
    @Override
    public void updateMerchantFree(String merchantKey, BigDecimal free) {
        freeDao.updateMerchantFree(merchantKey, free);
    }

}
