package com.learning.modules.sys.dao.system;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.learning.modules.sys.entity.system.SysUserRoleEntity;

import java.util.List;

/**
 * <p>
 *
 *    用户与角色对应关系
 *
 * @description: </p>
 * @author: Kevin
 * @since: Created in 下午8:08 2018/3/31
 */
public interface SysUserRoleDao extends BaseMapper<SysUserRoleEntity> {
	
	/**
	 * 根据用户ID，获取角色ID列表
	 */
	List<Long> queryRoleIdList(Long userId);

	/**
	 * 根据角色ID数组，批量删除
	 */
	int deleteBatch(Long[] roleIds);
}
