package com.learning.modules.sys.dao.system;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.learning.modules.sys.entity.system.SysMenuEntity;

import java.util.List;


/**
 * <p>
 *
 *    菜单管理
 *
 * @description: </p>
 * @author: Kevin
 * @since: Created in 下午8:08 2018/3/31
 */
public interface SysMenuDao extends BaseMapper<SysMenuEntity> {
	
	/**
	 * 根据父菜单，查询子菜单
	 * @param parentId 父菜单ID
	 */
	List<SysMenuEntity> queryListParentId(Long parentId);
	
	/**
	 * 获取不包含按钮的菜单列表
	 */
	List<SysMenuEntity> queryNotButtonList();

}
