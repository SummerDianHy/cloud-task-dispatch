package com.learning.modules.sys.redis;


import com.learning.common.utils.RedisKeys;
import com.learning.common.utils.RedisUtils;
import com.learning.modules.sys.entity.system.SysConfigEntity;
import org.springframework.stereotype.Component;

import org.springframework.beans.factory.annotation.Autowired;


/**
 * <p>
 *
 *    redis 配置
 *
 * @description: </p>
 * @author: Kevin
 * @since: Created in 下午8:08 2018/3/31
 */
@Component
public class SysConfigRedis {

    @Autowired
    private RedisUtils redisUtils;

    public void saveOrUpdate(SysConfigEntity config) {
        if(config == null){
            return ;
        }
        String key = RedisKeys.getSysConfigKey(config.getParamKey());
        redisUtils.set(key, config);
    }

    public void delete(String configKey) {
        String key = RedisKeys.getSysConfigKey(configKey);
        redisUtils.delete(key);
    }

    public SysConfigEntity get(String configKey){
        String key = RedisKeys.getSysConfigKey(configKey);
        return redisUtils.get(key, SysConfigEntity.class);
    }
}
