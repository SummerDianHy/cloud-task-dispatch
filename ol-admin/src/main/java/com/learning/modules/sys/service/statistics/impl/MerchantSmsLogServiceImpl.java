package com.learning.modules.sys.service.statistics.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.learning.datasources.DataSourceNames;
import com.learning.datasources.annotation.DataSource;
import com.learning.modules.sys.dao.statistics.MerchantSMSLogFreeDao;
import com.learning.modules.sys.entity.statistics.MerchantSmsLog;
import com.learning.modules.sys.service.statistics.IMerchantSmsLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * CDN费用发生记录 实现层
 *
 * @description: </p>
 * @author: JunAo.Yuan
 * @since: Created in 下午8:55 2018/11/10
 */
@Service
public class MerchantSmsLogServiceImpl extends ServiceImpl<MerchantSMSLogFreeDao, MerchantSmsLog> implements IMerchantSmsLogService {

    /**
     * 插入短信使用记录
     * @return
     */
    @DataSource(name = DataSourceNames.THIRD)
    @Override
    public void insertBatchs(List<MerchantSmsLog> smsLogList){
        this.insertBatch(smsLogList);
    }
}
