package com.learning.modules.sys.service.statistics;

import com.baomidou.mybatisplus.service.IService;
import com.learning.modules.sys.entity.statistics.MerchantSmsLog;

import java.util.List;

/**
 * <p>
 *     费用统计 接口层
 *
 */
public interface IMerchantSmsLogService extends IService<MerchantSmsLog> {

    void insertBatchs(List<MerchantSmsLog> smsLogList);
}
