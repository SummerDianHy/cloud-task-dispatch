package com.learning.modules.sys.service.system;

import com.baomidou.mybatisplus.service.IService;
import com.learning.modules.sys.entity.system.SysUserRoleEntity;

import java.util.List;

/**
 * <p>
 *
 *    用户与角色对应的关系
 *
 * @description: </p>
 * @author: Kevin
 * @since: Created in 下午8:08 2018/3/31
 */
public interface SysUserRoleService extends IService<SysUserRoleEntity> {
	
	void saveOrUpdate(Long userId, List<Long> roleIdList);
	
	/**
	 * 根据用户ID，获取角色ID列表
	 */
	List<Long> queryRoleIdList(Long userId);

	/**
	 * 根据角色ID数组，批量删除
	 */
	int deleteBatch(Long[] roleIds);
}
