package com.learning.modules.sys.dao.statistics;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.learning.modules.sys.entity.statistics.StatisticsAdminDaily;
import com.learning.modules.sys.entity.statistics.StatisticsOrgDaily;

/**
 * <p>
 *     企业统计 dao
 *
 * @description: </p>
 * @author: Kevin
 * @since: Created in 下午2:03 2018/5/15
 */
public interface StatisticsOrgDailyDao extends BaseMapper<StatisticsOrgDaily> {
}
