package com.learning.modules.oss.dao;


import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.learning.modules.oss.entity.SysOssEntity;


/**
 * <p>
 *
 *    文件上传
 *
 * @description: </p>
 * @author: Kevin
 * @since: Created in 下午8:08 2018/3/31
 */
public interface SysOssDao extends BaseMapper<SysOssEntity> {
	
}
