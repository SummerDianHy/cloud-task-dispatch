package com.learning.modules.oss.service;

import com.baomidou.mybatisplus.service.IService;
import com.learning.common.utils.PageUtils;
import com.learning.modules.oss.entity.SysOssEntity;

import java.util.Map;

/**
 * <p>
 *
 *    文件上传
 *
 * @description: </p>
 * @author: Kevin
 * @since: Created in 下午8:08 2018/3/31
 */
public interface SysOssService extends IService<SysOssEntity> {

	PageUtils queryPage(Map<String, Object> params);
}
