package com.learning.datasources;


/**
 * <p>
 *
 *    增加多数据源,在此处配置
 *
 * @description: </p>
 * @author: Kevin
 * @since: Created in 下午8:08 2018/3/31
 */
public interface DataSourceNames {
    String FIRST = "first";
    String SECOND = "second";//企业端
    String THIRD = "third";//平台端

}
