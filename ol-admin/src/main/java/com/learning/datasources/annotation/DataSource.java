package com.learning.datasources.annotation;

import java.lang.annotation.*;


/**
 * <p>
 *
 *    多数据源注解
 *
 * @description: </p>
 * @author: Kevin
 * @since: Created in 下午8:08 2018/3/31
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface DataSource {
    String name() default "";
}
