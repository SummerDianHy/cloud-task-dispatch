package com.learning.service;

import com.learning.datasources.DataSourceNames;
import com.learning.datasources.annotation.DataSource;
import com.learning.modules.sys.entity.system.SysUserEntity;
import com.learning.modules.sys.service.system.SysUserService;
import org.springframework.stereotype.Service;

import org.springframework.beans.factory.annotation.Autowired;

/**
 * 多数据源测试i
 */
@Service
public class DataSourceTestService {

    @Autowired
    private SysUserService sysUserService;


    public SysUserEntity queryUser(Long userId){
        return sysUserService.selectById(userId);
    }

    @DataSource(name = DataSourceNames.SECOND)
    public SysUserEntity queryUser2(Long userId){
        return sysUserService.selectById(userId);
    }

}
