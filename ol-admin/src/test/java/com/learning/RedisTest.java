package com.learning;

import com.learning.common.utils.RedisUtils;
import com.learning.modules.sys.entity.system.SysUserEntity;
import com.learning.modules.sys.service.base.IBaseService;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import org.springframework.beans.factory.annotation.Autowired;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RunWith(SpringRunner.class)
@SpringBootTest
public class RedisTest {

    private static final Logger logger = LoggerFactory.getLogger(RedisTest.class);

    @Autowired
    private RedisUtils redisUtils;

    @Autowired
    private IBaseService baseService;

    @Test
    public void contextLoads() {
        SysUserEntity user = new SysUserEntity();
        user.setEmail("123456@qq.com");
        redisUtils.set("user", user);

        System.out.println(ToStringBuilder.reflectionToString(redisUtils.get("user", SysUserEntity.class)));
    }

    @Test
    public void test() {
        List<HashMap<String, Object>> list = baseService.countCourseIncome();
        logger.info("测试结果集: {}", list);
    }

}
